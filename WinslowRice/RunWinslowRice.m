function [t,s] = RunWinslowRice()


%initial values
y0(1:4) = [-0.957193630176207   0.000006361264668   0.009986431317217   0.009988362919856];
y0(5:8) = [0.002628780262703   0.000001476470475   0.000000380034670   0.009999997975331];
y0(9:12) = [1.587590706671424   0.000000847203564   0.002620962386033   0.000001316332718];
y0(13:16) = [0.002617074968496   0.004929272613065   0.000006026472982   0.000000000028882];
y0(17:20) = [ 0.005064727425781   0.009980150290912   0.000000020069863   0.000000000043481];
y0(21:24) = [-0.000000000027157   0.000000000005919  -0.000000000000039   0.000019690534061];
y0(25:28) = [0.000000000158633   0.000000000000007  -0.000000000000006   0.000000000000002];
y0(29:30) = [0.007951684006957   0.009760253648343];

y0 = y0*100;

%% Rice model
N_NoXB = 1.0;
P_NoXB = 0.0;
N = 0.97;

XBprer = 3.0494964880038e-7;
XBpostr = 1.81017564383744e-6;
SL = 1.89;
xXBpostr = 0.00700005394873882;
xXBprer = 3.41212828972468e-8; 
TRPNCaL = 0.0147730085063734;
TRPNCaH = 0.13066096561522;
force = -4.5113452510363e-6;
intf = 0;

P = 1-N-XBprer-XBpostr;

y0 = [y0';N_NoXB; P_NoXB; N; P; XBprer; XBpostr; SL; xXBpostr; xXBprer; TRPNCaL; TRPNCaH; force; intf;0.1]; 

load merged.mat

konFac = x(1);
koffFac = x(2);
k_on = x(3);
k_off = x(4);
konFac2 = x(5);
koffFac2 = x(6);
sigman = x(7);
sigmap = x(8);

beats = 200;

for n = 1:beats 
    n
    options = odeset('MaxStep',0.5,'AbsTol',1e-10);%,'MaxStep',0.1);
    [t,y] = ode15s(@WinslowRice,[0:0.1:1000],y0,options,konFac,koffFac,k_on,k_off,konFac2,koffFac2,sigman,sigmap,1);
    y0 = (y(end,:));

end

save WR_RefOpti

