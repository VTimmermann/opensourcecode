function output = WinslowRice(t,z,konFac, koffFac,k_on, k_off,konFac2, koffFac2, sigman, sigmap, flag_ode)
%%%%%%%%%%%%%%%%%%%%%
%     Variables     %
%%%%%%%%%%%%%%%%%%%%%
dz = zeros(size(z));

% State Variables
V       = z(1);
m       = z(2);
h       = z(3);
j       = z(4);
xKr     = z(5);
xKs     = z(6);
xto1    = z(7);
yto1    = z(8);
K_i     = z(9);
Ca_i    = z(10);
Ca_NSR  = z(11);
Ca_ss   = z(12);
Ca_JSR  = z(13);
C1_RyR  = z(14);
O1_RyR  = z(15);
O2_RyR  = z(16);
C2_RyR  = z(17);
C0      = z(18); 
C1      = z(19);
C2      = z(20);
C3      = z(21);
C4      = z(22);
Open    = z(23);
CCa0    = z(24);
CCa1    = z(25);
CCa2    = z(26);
CCa3    = z(27);
CCa4    = z(28);
yCa     = z(29);
HTRPNCa = z(30);

LTRPNCa = z(44);


%% Rice Model
N_NoXB             = z(31);
P_NoXB             = z(32);
N                  = z(33);
P                  = z(34);
XBprer             = z(35);
XBpostr            = z(36);
SL                 = z(37);
xXBpostr           = z(38);
xXBprer            = z(39);
TRPNCaL            = z(40);
TRPNCaH            = z(41);
force              = z(42);
intf               = z(43);

%% Rice Model
% Sarcomere Geometry
SLmax   = 2.4;        % (um) maximum sarcomere length
SLmin   = 1.4;        % (um) minimum sarcomere length
SLset   = 1.89;
len_thin  = 1.2;      % (um) thin filament length
len_thick = 1.65;     % (um) thick filament length
len_hbare = 0.1;      % (um) length of bare portion of thick filament

% Temperature Dependence
Qkon = 1.5;
Qkoff = 1.3;
Qkn_p = 1.6;
Qkp_n = 1.6;
Qfapp = 6.25;
Qgapp = 2.5;
Qhf = 6.25;
Qhb = 6.25;
Qgxb = 6.25;

% Ca binding to troponin
kon     = 50e-3.*konFac;      % (1./[ms uM])
koffL   = 250e-3.*koffFac;     % (1./ms)
koffH   = 25e-3;      % (1./ms)
perm50  = 0.45;       % perm variable that controls n to p transition
nperm   = 10;         %   in Hill-like fashion
kn_p    = 500e-3;     % (1./ms)
kp_n    = 50e-3;      % (1./ms)
koffmod = 0.9;        % mod to change species

% Thin filament regulation and crossbridge cycling
fapp    = 500e-3;     % (1./ms) XB on rate
gapp    = 70e-3;      % (1./ms) XB off rate
gslmod  = 6;          % controls SL effect on gapp
hfXB    = 2000e-3;    % (1./ms) rate between pre-force and force states

%%
hfmdc   = 5;          % 
hb      = 400e-3;     % (1./ms) rate between pre-force and force states
hbmdc   = 0;          % 
gxb     = 70e-3;      % (1./ms) ATP consuming transition rate
% sigmap  = 8;          % distortion dependence of STP using transition gxb
% sigman  = 1;     % 
xbmodsp = 0.2;        % rabbit specific modification for XB cycling rates

% Mean strain of strongly-bound states
x_0     = 0.007;      % (um) strain induced by head rotation
xPsi    = 2;          % scaling factor balancing SL motion and XB cycling

% Normalized active and passive force
SLrest  = 1.89;       % (um) rest SL length for 0 passive force
PCon_t  = 0.002;      % (norm Force) passive force due to titin
PExp_t  = 10;         %   these apply to trabeculae and single cells only
SL_c    = 2.25;       % (um) resting length for collagen
PCon_c  = 0.02;       % (norm Force) passive force due to collagen
PExp_c  = 70;         %   these apply to trabeculae and single cells only


% Calculation of complete muscle response
massf   = 0.00025e6;  % ([norm Force ms.^2]./um) muscle mass
visc    = 0.003e3;    % ([norm Force ms]./um) muscle viscosity
KSE     = 1;          % (norm Force./um) series elastic element
kxb     = 120;        % (mN./mm.^2) maximal force
Trop_conc = 70;       % (uM) troponin concentration


%% Rice Model
% Time-Varying Parameters
CaiMech   = Ca_i.*1000;        %cytosolic calcium concentration from EP model
Temp      = 310;            % Temperature (K)

% Compute single overlap fractions
sovr_ze   = min(len_thick./2,SL./2);            % z-line end
sovr_cle  = max(SL./2-(SL-len_thin),len_hbare./2); % centerline of end
len_sovr  = sovr_ze-sovr_cle;                   % single overlap length
SOVFThick = len_sovr.*2./(len_thick-len_hbare); % thick filament overlap frac
SOVFThin  = len_sovr./len_thin;                 % thin filament overlap frac

% Compute combined Ca binding to high- (w./XB) and low- (no XB) sites
Tropreg = (1-SOVFThin).*TRPNCaL + SOVFThin.*TRPNCaH;
permtot = sqrt(1./(1+(perm50./Tropreg).^nperm));
inprmt  = min(1./permtot, 100);

% Adjustments for Ca activation, temperature, SL, stress and strain
konT    = kon.*Qkon.^((Temp-310)./10);
hfmd    = exp(-sign(xXBprer).*hfmdc.*((xXBprer./x_0).^2));
hbmd    = exp(sign((xXBpostr-x_0)).*hbmdc.*(((xXBpostr-x_0)./x_0).^2));

% Adjustments for Ca activation, temperature, SL, stress and strain
kn_pT   = kn_p.*permtot.*Qkn_p.^((Temp-310)./10);
kp_nT   = kp_n.*inprmt.*Qkp_n.^((Temp-310)./10);
fappT   = fapp.*xbmodsp.*Qfapp.^((Temp-310)./10);
gapslmd = 1 + (1-SOVFThick).*gslmod;
gappT   = gapp.*gapslmd.*xbmodsp.*Qgapp.^((Temp-310)./10);
hfT     = hfXB.*hfmd.*xbmodsp.*Qhf.^((Temp-310)./10);
hbT     = hb.*hbmd.*xbmodsp.*Qhb.^((Temp-310)./10);
koffLT  = koffL.*Qkoff.^((Temp-310)./10).*koffmod;
koffHT  = koffH.*Qkoff.^((Temp-310)./10).*koffmod;

% steady-state fractions in XBprer and XBpostr using King-Altman rule
SSXBprer = (hb.*fapp+gxb.*fapp)./...
  (gxb.*hfXB+fapp.*hfXB+gxb.*gapp+hb.*fapp+hb.*gapp+gxb.*fapp);
SSXBpostr = fapp.*hfXB./(gxb.*hfXB+fapp.*hfXB+gxb.*gapp+hb.*fapp+hb.*gapp+gxb.*fapp);

% normalization for scaling active and passive force (maximal force)
Fnordv = kxb.*x_0.*SSXBpostr;

% Calculate Forces (active, passive, preload, afterload)
force = kxb.*SOVFThick.*(xXBpostr.*XBpostr+xXBprer.*XBprer);
active = force./Fnordv;
ppforce_t = sign(SL-SLrest).*PCon_t.*(exp(PExp_t.*abs(SL-SLrest))-1);
if (SL-SL_c) < 0 
        ppforce_c = 0.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
elseif (SL-SL_c) == 0 
    ppforce_c = 1./2.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
else
    ppforce_c = 1.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
end
ppforce = ppforce_t + ppforce_c;
preload = sign(SLset-SLrest).*PCon_t.*(exp(PExp_t.*abs(SLset-SLrest))-1);
afterload = zeros(size(SL));%KSE./100.*(SLset-SL);

%% change in SL and forces
lmbda = SL./SLset;

dintf = (-ppforce+preload-active+afterload);
dSL = 0;


sigma = 6;
if t<=1040
    Gauss = 1040;
    GaussCurve = -1./(sqrt(8.9.*pi)) .* (-1/2.*(2*(t-(Gauss))./sigma)*1./sigma) .* exp(-1/2.*((t-(Gauss))./sigma).^2);
elseif t>1040 && t<=1240
    GaussCurve = 0;
else
    Gauss = 1240;
    GaussCurve = -1./(sqrt(8.9.*pi)) .* (-1/2.*(2*(t-(Gauss))./sigma)*1./sigma) .* exp(-1/2.*((t-(Gauss))./sigma).^2);
end

dSL = 0;%dSL + GaussCurve;

%% change to Rice model
sigman = sigman.*exp(konFac2*(lmbda-1));
sigmap = sigmap.*exp(koffFac2*(lmbda-1));
gxbmd   = heaviside(x_0-xXBpostr).*exp(sigmap.*((x_0-xXBpostr)./x_0).^2)+...
  (1-heaviside(x_0-xXBpostr)).*exp(sigman.*(((xXBpostr-x_0)./x_0).^2));

gxbT    = gxb.*gxbmd.*xbmodsp.*Qgxb.^((Temp-310)./10);

%%
% Regulation and corssbridge cycling state derivatives
dTRPNCaL  = konT.*(exp(k_on*(lmbda-1))).*CaiMech.*(1-TRPNCaL) - koffLT.*(exp(k_off*(lmbda-1))^(-1)).*TRPNCaL;
dTRPNCaH  = konT.*(exp(k_on*(lmbda-1))).*CaiMech.*(1-TRPNCaH) - koffHT.*(exp(k_off*(lmbda-1))^(-1)).*TRPNCaH;
%%

dN_NoXB   = -kn_pT.*N_NoXB+ kp_nT.*P_NoXB;
dP_NoXB   = -kp_nT.*P_NoXB + kn_pT.*N_NoXB;

dN        = -kn_pT.*N+ kp_nT.*P;
% dP      = -kp_nT.*P + kn_pT.*N - fappT.*P + gappT.*XBprer + gxbT.*XBpostr;
dXBprer   = fappT.*P - gappT.*XBprer - hfT.*XBprer + hbT.*XBpostr;
dXBpostr  = hfT.*XBprer - hbT.*XBpostr - gxbT.*XBpostr;

dP        = -(dN+dXBprer+dXBpostr);

% Mean strain of strongly-bound states due to SL motion and XB cycling
dutyprer  = (hbT.*fappT+gxbT.*fappT)./...    % duty fractions using the
  (fappT.*hfT+gxbT.*hfT+gxbT.*gappT+hbT.*fappT+hbT.*gappT+gxbT.*fappT);
dutypostr = fappT.*hfT./...                 % King-Alman Rule    
  (fappT.*hfT+gxbT.*hfT+gxbT.*gappT+hbT.*fappT+hbT.*gappT+gxbT.*fappT);
dxXBprer = 0.5.*dSL+xPsi./dutyprer.*(-xXBprer.*fappT+(xXBpostr-x_0-xXBprer).*hbT);
dxXBpostr = 0.5.*dSL+ xPsi./dutypostr.*(x_0+xXBprer-xXBpostr).*hfT;

% Ca buffering by low-affinity troponin C (LTRPNCa)
FrSBXB    = (XBpostr+XBprer)./(SSXBpostr + SSXBprer);
dFrSBXB   = (dXBpostr+dXBprer)./(SSXBpostr + SSXBprer);

if (len_thick-SL) < 0 
    dsovr_ze  = -dSL./2.*0;
elseif (len_thick-SL) == 0 
    dsovr_ze  = -dSL./2.*1./2;
else
    dsovr_ze  = -dSL./2.*1;
end

if ((2.*len_thin-SL)-len_hbare) < 0
    dsovr_cle = -dSL./2.*0;
elseif ((2.*len_thin-SL)-len_hbare) == 0
    dsovr_cle = -dSL./2.*1./2;
else
    dsovr_cle = -dSL./2.*1;
end

dlen_sovr = dsovr_ze-dsovr_cle;
dSOVFThin = dlen_sovr./len_thin;
dSOVFThick= 2.*dlen_sovr./(len_thick-len_hbare);

TropTot = Trop_conc.*((1-SOVFThin).*TRPNCaL + ...
  SOVFThin.*(FrSBXB.*TRPNCaH+(1-FrSBXB).*TRPNCaL));
dTropTot= (Trop_conc.*(-dSOVFThin.*TRPNCaL+(1-SOVFThin).*dTRPNCaL + ...
  dSOVFThin.*(FrSBXB.*TRPNCaH+(1-FrSBXB).*TRPNCaL) + ...
  SOVFThin.*(dFrSBXB.*TRPNCaH+FrSBXB.*dTRPNCaH-dFrSBXB.*TRPNCaL+...
  (1-FrSBXB).*dTRPNCaL)));


dforce = kxb.*dSOVFThick.*(xXBpostr.*XBpostr+xXBprer.*XBprer) + ...
  kxb.*SOVFThick.*(dxXBpostr.*XBpostr+xXBpostr.*dXBpostr + ...
  dxXBprer.*XBprer+xXBprer.*dXBprer);

dactive = 0.5.*dforce./Fnordv;
dppforce_t = sign(SL-SLrest).*PCon_t.*PExp_t.*dSL.*exp(PExp_t.*abs(SL-SLrest));

if (SL-SL_c) < 0
    dppforce_c = 0.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
elseif (SL-SL_c) == 0
    dppforce_c = 1./2.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
else
    dppforce_c = 1.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
end

dppforce = dppforce_t + dppforce_c;
dsfib = dppforce+dactive;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Cell geometry parameters
C_sc  = 1.00;
A_cap = 1.534e-4;
V_myo = 25.84e-6;
V_JSR = 0.16e-6;
V_NSR = 2.1e-6;
V_ss  = 1.2e-9;

%Standard ionic concentrations

K_o  = 4.0;
Na_o = 138.0;
Ca_o = 2.0;
Na_i = 10.0;

%Membrane current parameters

F       = 96.500;
T       = 310;
R       = 8.314; 
RTonF   = R*T/F;
FonRT   = F/(R*T);
G_KrMax = 0.0034;
G_KsMax = 0.00271;
G_toMax = 0.23815;
G_tiMax = 2.8;
G_KpMax = 0.002216;
G_NaMax = 12.8;
k_NaCa  = 0.30;
K_mNa   = 87.5;
K_mCa   = 1.38;
K_mK1   = 13.0;
k_sat   = 0.2;
eta     = 0.35;
I_NaKMax= 0.693;
K_mNai  = 10.0;
K_mKo   = 1.5;
I_pCaMax= 0.05;
K_mpCa  = 0.00005;
G_bCaMax= 0.0003842;
G_bNaMax= 0.0031; 

%SR parameters
v_1     = 3.6; %changed from 1.8;
K_fb    = 0.168e-3;
K_rb    = 3.29;
K_SR    = 1.5;%1.0 in original Winslow mod;
N_fb    = 1.2;
N_rb    = 1.0;
v_maxf  = 0.813e-4;
v_maxr  = 0.318e-3;
tau_tr  = 0.5747;
tau_xfer= 20.7; %26.7
kaplus = 0.01215;
kaminus= 0.576;
kbplus = 0.00405;
kbminus= 1.930;
kcplus = 0.100;
kcminus= 0.0008;
ncoop   = 4.0;
mcoop   = 3.0;

%L-type Ca Channel parameters

fL     = 0.3;
gL     = 2.0;
bL     = 2.0;
aL     = 2.0;
omega   = 0.01;
P_scale = 1.0;
PCa = P_scale*3.125e-4;
PK  = P_scale*5.79e-7;
ICahalf= -0.265;

%Buffering parameters

LTRPNtot   = 70e-3;
HTRPNtot   = 140e-3;
khtrpn_plus = 20.0;
khtrpn_minus= 66.0e-6;
kltrpn_plus = 40.0;
kltrpn_minus= 0.163;%taken from HMT, adjusted below
CMDNtot    = 50e-3;
CSQNtot    = 15.0;
EGTAtot    = 10.0;
KmCMDN     = 2.38e-3;
KmCSQN     = 0.8;
KmEGTA     = 0.15e-3;

TRPN_n = 2;

VFonRT = V*FonRT;
expVFonRT = exp(VFonRT);


% I Membrane currents

%Na+ current I_Na

E_Na = RTonF*log(Na_o/Na_i);
I_Na = G_NaMax*m^3*h*j*(V-E_Na);

if (V>=-40)
   a_h = 0;
   b_h = 1/(0.13*(1+exp((V+10.66)/(-11.1))));
   a_j = 0;
   b_j = 0.3*exp(-2.535e-7*V)/(1+exp(-0.1*(V+32)));
else
   a_h = 0.135*exp((80+V)/(-6.8));
   b_h = 3.56*exp(0.079*V)+3.1e5*exp(0.35*V);
   a_j = (-1.2714e5*exp(0.2444*V)-3.474e-5*exp(-0.04391*V))...
	 *(V+37.78)/(1+exp(0.311*(V+79.23)));
   b_j = 0.1212*exp(-0.01052*V)/(1+exp(-0.1378*(V+40.14)));
end


if (abs(V+47.13)<=1e-3)
  a_m = 1.0/(0.1-0.005*(V+47.13));
else
  a_m = 0.32*(V+47.13)/(1-exp(-0.1*(V+47.13)));
end
  
b_m = 0.08*exp(-V/11);

if (V >= -90)
  dm = a_m*(1-m)-b_m*m;
else
  dm = 0;
  m = a_m/(a_m+b_m);
end


%Rapid-activating delayed rectifier K+ current I_Kr
k12 = exp(-5.495+0.1691*V);
k21 = exp(-7.677-0.0128*V);
xKr_inf = k12/(k12+k21);
tau_xKr = 1.0/(k12+k21) + 27.0;
dxKr = (xKr_inf-xKr)/tau_xKr;


E_k  = RTonF*log(K_o/K_i);
R_V  = 1/(1+1.4945*exp(0.0446*V));
f_k  = sqrt(K_o/4);

I_Kr = G_KrMax*f_k*R_V*xKr*(V-E_k);

%Slow-activating delayed rectifier K+ current I_Ks
xKs_inf = 1.0/(1.0+exp(-(V-24.70)/13.60));
a1 = 7.19e-5*(V-10.0)/(1.0-exp(-0.1480*(V-10.0)));
a2 = 1.31e-4*(V-10.0)/(exp(0.06870*(V-10.0))-1.0);
tau_xKs = 1.0/(a1+a2);
dxKs = (xKs_inf-xKs)/tau_xKs;

E_Ks = RTonF*log((K_o+0.01833*Na_o)/(K_i+0.01833*Na_i));

I_Ks = G_KsMax*(xKs^2)*(V-E_Ks);


%Transient outward K+ current I_to
alpha_xto1 = 0.04516*exp(0.03577*V);
beta_xto1  = 0.0989*exp(-0.06237*V);
a1 = 1.0+0.051335*exp(-(V+33.5)/5.0);
alpha_yto1 = 0.005415*exp(-(V+33.5)/5.0)/a1;
a1 = 1.0+0.051335*exp((V+33.5)/5.0);
beta_yto1 = 0.005415*exp((V+33.5)/5.0)/a1;

dxto1 = alpha_xto1*(1.d0-xto1)-beta_xto1*xto1;
dyto1 = alpha_yto1*(1.d0-yto1)-beta_yto1*yto1;

I_to = G_toMax*xto1*yto1*(V-E_k);


%Time-Independent K+ current I_ti
K_ti_ss = 1/(2+exp(1.5*(V-E_k)*FonRT));

I_ti = G_tiMax*K_ti_ss*(K_o/(K_o+K_mK1))*(V-E_k);

%Plateau current I_Kp

K_p  = 1/(1+exp((7.488-V)/5.98));
I_Kp = G_KpMax*K_p*(V-E_k);

%NCX Current I_NaCa
I_NaCa = k_NaCa*(5000/(K_mNa^3+Na_o^3))*(1/(K_mCa+Ca_o))...
         *(1/(1+k_sat*exp((eta-1)*V*FonRT)))*(exp(eta*VFonRT)...
         *Na_i^3*Ca_o-exp((eta-1)*VFonRT)*Na_o^3*Ca_i);


%Na+-K+ pump current I_NaK
sigma = 1/7*(exp(Na_o/67.3)-1);
f_NaK = 1/(1+0.1245*exp(-0.1*VFonRT)+0.0365*sigma*exp(-VFonRT));

I_NaK = I_NaKMax*f_NaK*1/(1+(K_mNai/Na_i)^1.5)*K_o/(K_o+K_mKo);


%Sarcolemmal Ca2+ pump current I_pCa
I_pCa = 2.0*I_pCaMax*Ca_i/(K_mpCa+Ca_i);

%Ca2+ background current I_bCa
E_Ca  = RTonF/2*log(Ca_o/Ca_i);
I_bCa = G_bCaMax*(V-E_Ca);


%Na+ background current I_bNa
I_bNa = G_bNaMax*(V-E_Na);

% II Ca2+ handling mechanisms

%L-type Ca2+ current I_Ca
alpha      = 0.4*exp((V+2.0)/10.0);
beta       = 0.05*exp(-(V+2.0)/13.0);
alpha_prime = alpha * aL;
beta_prime  = beta/bL;
gamma = 0.10375*Ca_ss;
        
C0_to_C1 = 4.d0*alpha;
C1_to_C2 = 3.d0*alpha;
C2_to_C3 = 2.d0*alpha;
C3_to_C4 =      alpha;

CCa0_to_CCa1 = 4.d0*alpha_prime;
CCa1_to_CCa2 = 3.d0*alpha_prime;
CCa2_to_CCa3 = 2.d0*alpha_prime;
CCa3_to_CCa4 =      alpha_prime;

C1_to_C0 =      beta;
C2_to_C1 = 2.d0*beta;
C3_to_C2 = 3.d0*beta;
C4_to_C3 = 4.d0*beta;

CCa1_to_CCa0 =      beta_prime;
CCa2_to_CCa1 = 2.d0*beta_prime;
CCa3_to_CCa2 = 3.d0*beta_prime;
CCa4_to_CCa3 = 4.d0*beta_prime;
		
gamma =   0.10375d0*Ca_ss;

C0_to_CCa0 = gamma;		
C1_to_CCa1 = aL*C0_to_CCa0;	
C2_to_CCa2 = aL*C1_to_CCa1;	
C3_to_CCa3 = aL*C2_to_CCa2;	
C4_to_CCa4 = aL*C3_to_CCa3;	
		
CCa0_to_C0 = omega;		
CCa1_to_C1 = CCa0_to_C0/bL;	
CCa2_to_C2 = CCa1_to_C1/bL;	
CCa3_to_C3 = CCa2_to_C2/bL;	
CCa4_to_C4 = CCa3_to_C3/bL;	

a1 = (C0_to_C1+C0_to_CCa0)*C0;
a2 = C1_to_C0*C1 + CCa0_to_C0*CCa0;
dC0 = a2 - a1;

a1 = (C1_to_C0+C1_to_C2+C1_to_CCa1)*C1;
a2 = C0_to_C1*C0 + C2_to_C1*C2 + CCa1_to_C1*CCa1;
dC1 = a2 - a1;

a1 = (C2_to_C1+C2_to_C3+C2_to_CCa2)*C2;
a2 = C1_to_C2*C1 + C3_to_C2*C3 + CCa2_to_C2*CCa2;
dC2 = a2 - a1;

a1 = (C3_to_C2+C3_to_C4+C3_to_CCa3)*C3;
a2 = C2_to_C3*C2 + C4_to_C3*C4 + CCa3_to_C3*CCa3;
dC3 = a2 - a1;

a1 = (C4_to_C3+fL+C4_to_CCa4)*C4;
a2 = C3_to_C4*C3 + gL*Open + CCa4_to_C4*CCa4;
dC4 = a2 - a1;

dOpen =  fL*C4 - gL*Open;

a1 = (CCa0_to_CCa1+CCa0_to_C0)*CCa0;
a2 = CCa1_to_CCa0*CCa1 + C0_to_CCa0*C0;
dCCa0 = a2 - a1;

a1 = (CCa1_to_CCa0+CCa1_to_CCa2+CCa1_to_C1)*CCa1;
a2 = CCa0_to_CCa1*CCa0 + CCa2_to_CCa1*CCa2 + C1_to_CCa1*C1;
dCCa1 = a2 - a1;

a1 = (CCa2_to_CCa1+CCa2_to_CCa3+CCa2_to_C2)*CCa2;
a2 = CCa1_to_CCa2*CCa1 + CCa3_to_CCa2*CCa3 + C2_to_CCa2*C2;
dCCa2 = a2 - a1;

a1 = (CCa3_to_CCa2+CCa3_to_CCa4+CCa3_to_C3)*CCa3;
a2 = CCa2_to_CCa3*CCa2 + CCa4_to_CCa3*CCa4 + C3_to_CCa3*C3;
dCCa3 = a2 - a1;

a1 = (CCa4_to_CCa3+CCa4_to_C4)*CCa4;
a2 = CCa3_to_CCa4*CCa3 + C4_to_CCa4*C4;
dCCa4 = a2 - a1;

yCa_inf = 0.80/(1.0+exp((V + 12.5)/5.0)) + 0.2;
tau_yCa = 20.0 + 600.0 / (1.0 + exp((V+20.0)/9.50));
dyCa = (yCa_inf-yCa)/tau_yCa;

VFonRT=V/RTonF;
VFsqonRT=(1000.0*F)*VFonRT;

a1 =  1.0e-3*exp(2.0*VFonRT)-Ca_o*0.341; 
a2 =  exp(2.0*VFonRT)-1.0;
ICamax = PCa*4.0*VFsqonRT*(a1/a2);
I_Ca = ICamax*yCa*Open;

PKprime = PK/(1.0+(min(0.0,ICamax)/ICahalf ));  
a1 = K_i*exp(VFonRT)-K_o;
a2 = exp(VFonRT)-1.0;
I_CaK = PKprime*Open*yCa*VFsqonRT*(a1/a2);

%RyR Channel 
a1 = (Ca_ss*1000.0)^mcoop;
a2 = (Ca_ss*1000.0)^ncoop;
dC1_RyR = -kaplus*a2*C1_RyR+kaminus*O1_RyR;
dO2_RyR =  kbplus*a1*O1_RyR - kbminus*O2_RyR;
dC2_RyR =  kcplus*O1_RyR - kcminus*C2_RyR; 
dO1_RyR = -(dC1_RyR + dO2_RyR + dC2_RyR);

J_rel = v_1*(O1_RyR+O2_RyR)*(Ca_JSR-Ca_ss);


%SERCA2a Pump 
f_b = (Ca_i/K_fb)^N_fb;
r_b = (Ca_NSR/K_rb)^N_rb;

J_up = K_SR*(v_maxf*f_b-v_maxr*r_b)/(1+f_b+r_b);

%Intracellular Ca fluxes

J_tr = (Ca_NSR-Ca_JSR)/tau_tr;

J_xfer = (Ca_ss-Ca_i)/tau_xfer;


%Calcium binding to low affinity sites, from Rice model
a1 = kltrpn_minus *(1-1/2.6)*LTRPNCa;
% dLTRPNCa = kltrpn_plus*Ca_i*(1.0 - LTRPNCa) - a1;
dLTRPNCa = dTropTot/Trop_conc;

%Binding to high affinity, non-regulatory sites, from Winslow model
a1 = khtrpn_minus * HTRPNCa;
dHTRPNCa = khtrpn_plus*Ca_i*(1.0 - HTRPNCa) - a1;
	
%total calcium buffering by Troponin C
J_trpn = LTRPNtot*dLTRPNCa+HTRPNtot*dHTRPNCa;

%other (instantanous) Ca buffers; cmdn, csqn, egta
a1 = CMDNtot*KmCMDN/((Ca_ss+KmCMDN)^2.0);
a2 = 0;%EGTAtot*KmEGTA/((Ca_ss+KmEGTA)^2.0);
beta_ss = 1.0/(1.0+a1+a2); 
		
a1 = CSQNtot*KmCSQN/((Ca_JSR+KmCSQN)^2.0);
beta_JSR = 1.0/(1.0+a1);

a1 = CMDNtot*KmCMDN/((Ca_i+KmCMDN)^2.d0);
a2 = 0;%EGTAtot*KmEGTA/((Ca_i+KmEGTA)^2.d0);
beta_i = 1.0/(1.0+a1+a2);

%initial stimulating current I_st
I_st = 0;
t_mod = mod(t,1000);
if (t_mod>10.0&&t_mod<11)
I_st = -100;
end

dCai = beta_i*(J_xfer-J_up-(J_trpn)-(I_bCa-2*I_NaCa+I_pCa)...
        *A_cap*C_sc/(2*V_myo*1000*F));


% Update state derivatives
dz(1) = -(I_Na+I_Ca+I_CaK+I_Kr+I_Ks+I_to+I_ti+I_Kp...
        +I_NaCa+I_NaK+I_pCa+I_bCa+I_bNa+I_st);
dz(2) = dm;%a_m*(1-m)-b_m*m;
dz(3) = a_h*(1-h)-b_h*h;
dz(4) = a_j*(1-j)-b_j*j;
dz(5) = dxKr;
dz(6) = dxKs;
dz(7) = dxto1;
dz(8) = dyto1;
dz(9) = -(I_Kr+I_Ks+I_to+I_ti+I_Kp+I_CaK-2*I_NaK)*A_cap*C_sc/(V_myo*1000*F);
dz(10)= dCai;
dz(11)= J_up*V_myo/V_NSR-J_tr*V_JSR/V_NSR;
dz(12)= beta_ss*(J_rel*V_JSR/V_ss-J_xfer*V_myo/V_ss-I_Ca*A_cap...
        *C_sc/(2*V_ss*1000*F));
dz(13)= beta_JSR*(J_tr-J_rel);        
dz(14)= dC1_RyR;
dz(15)= dO1_RyR;
dz(16)= dO2_RyR;
dz(17)= dC2_RyR;
dz(18)= dC0;
dz(19)= dC1;
dz(20)= dC2;
dz(21)= dC3;
dz(22)= dC4;
dz(23)= dOpen;
dz(24)= dCCa0;
dz(25)= dCCa1;
dz(26)= dCCa2;
dz(27)= dCCa3;
dz(28)= dCCa4;
dz(29)= dyCa;
dz(30)= dHTRPNCa;

%dydt = zeros(size(z));
dz(31)   = dN_NoXB;
dz(32)     = dP_NoXB;
dz(33)     = dN;
dz(34)     = dP;
dz(35)     = dXBprer;
dz(36)     = dXBpostr;
dz(37)     = dSL;
dz(38)     = dxXBpostr;
dz(39)     = dxXBprer;
dz(40)     = dTRPNCaL;
dz(41)     = dTRPNCaH;
dz(42)     = dforce;
dz(43)     = dintf;
dz(44)     = dLTRPNCa;

if flag_ode==1
    output=[dz];
else
    output=[I_Na I_Ca I_CaK I_Kr I_Ks I_to I_ti I_Kp...
         I_NaCa I_NaK I_pCa I_bCa I_bNa I_st I_SAC I_ns I_K0];
end
