function PlotFunction2

load WR_125_TPConly.mat

subplot(5,1,1); hold on,plot(t(1:2:end),y(1:2:end,1)); title('Voltage');
subplot(5,1,2); hold on,plot(t(1:2:end),y(1:2:end,10)*1000); title('Cyto calcium');
subplot(5,1,3); hold on,plot(t(1:2:end),y(1:2:end,37)); title('sarcomere length');
subplot(5,1,4); hold on,plot(t(1:2:end),y(1:2:end,48)); title('TnC');
subplot(5,1,5); hold on,plot(t(1:2:end),y(1:2:end,11)); title('SR');

hold on
load WR_125_SAConly.mat

subplot(5,1,1); hold on,plot(t(1:2:end),y(1:2:end,1)); title('Voltage');
subplot(5,1,2); hold on,plot(t(1:2:end),y(1:2:end,10)*1000); title('Cyto calcium');
subplot(5,1,3); hold on,plot(t(1:2:end),y(1:2:end,37)); title('sarcomere length');
subplot(5,1,4); hold on,plot(t(1:2:end),y(1:2:end,48)); title('TnC');
subplot(5,1,5); hold on,plot(t(1:2:end),y(1:2:end,11)); title('SR');

load WR_125.mat

subplot(5,1,1); hold on,plot(t(1:2:end),y(1:2:end,1)); title('Voltage');
axis([0 700 -100 50])
subplot(5,1,2); hold on,plot(t(1:2:end),y(1:2:end,10)*1000); title('Cyto calcium');
axis([0 700 0 1])
subplot(5,1,3); hold on,plot(t(1:2:end),y(1:2:end,37)); title('sarcomere length');
axis([0 700 1.85 1.95])
subplot(5,1,4); hold on,plot(t(1:2:end),y(1:2:end,48)); title('TnC');
axis([0 700 0 0.35])
subplot(5,1,5); hold on,plot(t(1:2:end),y(1:2:end,11)); title('SR');
axis([0 700 0 0.3])