function RunShannonRice
%% Initial conditions
mo=1.405627e-3;
ho= 9.867005e-1;
jo=9.915620e-1; 
do=7.175662e-6; 
fo=1.000681; 
fcaBjo=2.421991e-2;
fcaBslo=1.452605e-2;
xtoso=4.051574e-3;
ytoso=9.945511e-1; 
xtofo=4.051574e-3; 
ytofo= 9.945511e-1; 
xkro=8.641386e-3; 
xkso= 5.412034e-3;
RyRro=8.884332e-1;
RyRoo=8.156628e-7; 
RyRio=1.024274e-7; 
NaBjo=3.539892;
NaBslo=7.720854e-1; 
TnCLo=8.773191e-3; 
TnCHco=1.078283e-1; 
TnCHmo=1.524002e-2; 
CaMo=2.911916e-4; 
Myoco=1.298754e-3; 
Myomo=1.381982e-1;
SRBo=2.143165e-3; 
SLLjo=9.566355e-3; 
SLLslo=1.110363e-1; 
SLHjo=7.347888e-3; 
SLHslo=7.297378e-2; 
Csqnbo= 1.242988;
Ca_sro=5.545201e-1;
Najo=8.80329; 
Naslo=8.80733; 
Naio=8.80853; 
Cajo=1.737475e-4; 
Caslo= 1.031812e-4; 
Caio=8.597401e-5; 
Vmo=-8.556885e+1; 
rtoso=0.9946; 

%% Rice model
N_NoXB = 1.0;
P_NoXB = 0.0;
N = 0.97;

XBprer = 3.0494964880038e-7;
XBpostr = 1.81017564383744e-6;
SL = 1.89;
xXBpostr = 0.00700005394873882;
xXBprer = 3.41212828972468e-8; 
TRPNCaL = 0.0147730085063734;
TRPNCaH = 0.13066096561522;
force = -4.5113452510363e-6;
intf = 0;

P = 1-N-XBprer-XBpostr;

%%
% Gating variables   
y00 = Vmo;

%    2       3       4       5       6       7         8       9       10      11      12     13     14
%%   m       h       j       d       f       fcaBj   fcaBsl   xtos    ytos    xtof    ytof    xkr    xks   
y10=[mo; ho; jo; do; fo; fcaBjo; fcaBslo; xtoso; ytoso; xtofo; ytofo; xkro; xkso;];  

% RyR and Buffering variables
%     15      16      17      18      19      20      21      22      23      24      25
%%   RyRr    RyRo    RyRi    NaBj    NaBsl   TnCL    TnCHc   TnCHm   CaM     Myoc    Myom  
y20=[RyRro; RyRoo; RyRio; NaBjo; NaBslo; TnCLo; TnCHco; TnCHmo; CaMo; Myoco; Myomo;];

% More buffering variables
%    26       27     28       29      30     31
%%   SRB     SLLj   SLLsl    SLHj    SLHsl  Csqnb
y30=[SRBo; SLLjo; SLLslo; SLHjo; SLHslo; Csqnbo];

% Intracellular concentrations./ Membrane voltage
%     32     33      34     35     36     37     38     39          
%%   Ca_sr   Naj    Nasl    Nai    Caj   Casl   Cai    rtos  
y40=[Ca_sro; Najo; Naslo; Naio; Cajo; Caslo; Caio; rtoso];    

% mechanics
%        40      41     42    43      44      45     46     47       48       49       50      51     52    53   
%%     N_NoXB  P_NoXB   N     P     XBprer  XBpostr  SL  xXBpostr  xXBprer  TRPNCaL  TRPNCaH  force  intf  curve
y50 = [N_NoXB; P_NoXB; N; P; XBprer; XBpostr; SL; xXBpostr; xXBprer; TRPNCaL; TRPNCaH; force; intf;]; 


%% Put everything together
y0 = [y00;y10;y20;y30;y40;y50];

load merged.mat

konFac = x(1);
koffFac = x(2);
k_on = x(3);
k_off = x(4);
konFac2 = x(5);
koffFac2 = x(6);
sigman = x(7);
sigmap = x(8);

for beats = 1:200
    beats
    options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
    [t,y] = ode15s(@ShannonRice,[0:0.1:1000],y0,options, konFac, koffFac, k_on, k_off,konFac2, koffFac2, sigman, sigmap);

    y0 = y(end,:);
end

save SR_RefOpti