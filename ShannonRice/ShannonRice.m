function output = ShannonRice(t,y,konFac, koffFac,k_on, k_off,konFac2, koffFac2, sigman, sigmap, runType)
    if isreal(y) == 0
        output = zeros(size(y));
    else
        ydot = zeros(size(y));

        %% EP model
        Vm                = y(1);
        m                 = y(2);
        h                 = y(3);
        j                 = y(4);
        d                 = y(5);
        f                 = y(6);
        fcaBj             = y(7);
        fcaBsl            = y(8);
        xtos              = y(9);
        ytos              = y(10);
        xtof              = y(11);
        ytof              = y(12);
        xkr               = y(13);
        xks               = y(14);
        RyRr              = y(15);
        RyRo              = y(16);
        RyRi              = y(17);
        NaBj              = y(18);
        NaBsl             = y(19);
        TnCL              = y(20);
        TnCHc             = y(21);
        TnCHm             = y(22);
        CaM               = y(23);
        Myoc              = y(24);
        Myom              = y(25);
        SRB               = y(26);
        SLLj              = y(27);
        SLLsl             = y(28);
        SLHj              = y(29);
        SLHsl             = y(30);
        Csqnb             = y(31);
        CaSR              = y(32);
        Naj               = y(33);
        Nasl              = y(34);
        Nai               = y(35);
        Caj               = y(36);
        CaSL              = y(37);
        Cai               = y(38);
        rtos              = y(39);

        %% Rice Model
        N_NoXB             = y(40);
        P_NoXB             = y(41);
        N                  = y(42);
        P                  = y(43);
        XBprer             = y(44);
        XBpostr            = y(45);
        SL                 = y(46);
        xXBpostr           = y(47);
        xXBprer            = y(48);
        TRPNCaL            = y(49);
        TRPNCaH            = y(50);
        force              = y(51);
        intf               = y(52);

        %% Rice Model
        % Sarcomere Geometry
        SLmax   = 2.4;        % (um) maximum sarcomere length
        SLmin   = 1.4;        % (um) minimum sarcomere length
        SLset   = 1.89;
        len_thin  = 1.2;      % (um) thin filament length
        len_thick = 1.65;     % (um) thick filament length
        len_hbare = 0.1;      % (um) length of bare portion of thick filament

        % Temperature Dependence
        Qkon = 1.5;
        Qkoff = 1.3;
        Qkn_p = 1.6;
        Qkp_n = 1.6;
        Qfapp = 6.25;
        Qgapp = 2.5;
        Qhf = 6.25;
        Qhb = 6.25;
        Qgxb = 6.25;

        % Ca binding to troponin
        kon     = 50e-3.*konFac;      % (1./[ms uM])
        koffL   = 250e-3.*koffFac;     % (1./ms)
        koffH   = 25e-3;      % (1./ms)
        perm50  = 0.45;       % perm variable that controls n to p transition
        nperm   = 10;         %   in Hill-like fashion
        kn_p    = 500e-3;     % (1./ms)
        kp_n    = 50e-3;      % (1./ms)
        koffmod = 0.9;        % mod to change species

        % Thin filament regulation and crossbridge cycling
        fapp    = 500e-3;     % (1./ms) XB on rate
        gapp    = 70e-3;      % (1./ms) XB off rate
        gslmod  = 6;          % controls SL effect on gapp
        hfXB    = 2000e-3;    % (1./ms) rate between pre-force and force states

        %%
        hfmdc   = 5;          % 
        hb      = 400e-3;     % (1./ms) rate between pre-force and force states
        hbmdc   = 0;          % 
        gxb     = 70e-3;      % (1./ms) ATP consuming transition rate
%         sigmap  = 8;          % distortion dependence of STP using transition gxb
%         sigman  = 1;     % 
        xbmodsp = 0.2;        % rabbit specific modification for XB cycling rates

        % Mean strain of strongly-bound states
        x_0     = 0.007;      % (um) strain induced by head rotation
        xPsi    = 2;          % scaling factor balancing SL motion and XB cycling

        % Normalized active and passive force
        SLrest  = 1.89;       % (um) rest SL length for 0 passive force
        PCon_t  = 0.002;      % (norm Force) passive force due to titin
        PExp_t  = 10;         %   these apply to trabeculae and single cells only
        SL_c    = 2.25;       % (um) resting length for collagen
        PCon_c  = 0.02;       % (norm Force) passive force due to collagen
        PExp_c  = 70;         %   these apply to trabeculae and single cells only


        % Calculation of complete muscle response
        massf   = 0.00025e6;  % ([norm Force ms.^2]./um) muscle mass
        visc    = 0.003e3;    % ([norm Force ms]./um) muscle viscosity
        KSE     = 1;          % (norm Force./um) series elastic element
        kxb     = 120;        % (mN./mm.^2) maximal force
        Trop_conc = 70;       % (uM) troponin concentration


        %% Rice Model
        % Time-Varying Parameters
        CaiMech   = Cai.*1000;        %cytosolic calcium concentration from EP model
        Temp      = 310;            % Temperature (K)

        % Compute single overlap fractions
        sovr_ze   = min(len_thick./2,SL./2);            % z-line end
        sovr_cle  = max(SL./2-(SL-len_thin),len_hbare./2); % centerline of end
        len_sovr  = sovr_ze-sovr_cle;                   % single overlap length
        SOVFThick = len_sovr.*2./(len_thick-len_hbare); % thick filament overlap frac
        SOVFThin  = len_sovr./len_thin;                 % thin filament overlap frac

        % Compute combined Ca binding to high- (w./XB) and low- (no XB) sites
        Tropreg = (1-SOVFThin).*TRPNCaL + SOVFThin.*TRPNCaH;
        permtot = sqrt(1./(1+(perm50./Tropreg).^nperm));
        inprmt  = min(1./permtot, 100);

        % Adjustments for Ca activation, temperature, SL, stress and strain
        konT    = kon.*Qkon.^((Temp-310)./10);
        hfmd    = exp(-sign(xXBprer).*hfmdc.*((xXBprer./x_0).^2));
        hbmd    = exp(sign((xXBpostr-x_0)).*hbmdc.*(((xXBpostr-x_0)./x_0).^2));

        % Adjustments for Ca activation, temperature, SL, stress and strain
        kn_pT   = kn_p.*permtot.*Qkn_p.^((Temp-310)./10);
        kp_nT   = kp_n.*inprmt.*Qkp_n.^((Temp-310)./10);
        fappT   = fapp.*xbmodsp.*Qfapp.^((Temp-310)./10);
        gapslmd = 1 + (1-SOVFThick).*gslmod;
        gappT   = gapp.*gapslmd.*xbmodsp.*Qgapp.^((Temp-310)./10);
        hfT     = hfXB.*hfmd.*xbmodsp.*Qhf.^((Temp-310)./10);
        hbT     = hb.*hbmd.*xbmodsp.*Qhb.^((Temp-310)./10);
        koffLT  = koffL.*Qkoff.^((Temp-310)./10).*koffmod;
        koffHT  = koffH.*Qkoff.^((Temp-310)./10).*koffmod;

        % steady-state fractions in XBprer and XBpostr using King-Altman rule
        SSXBprer = (hb.*fapp+gxb.*fapp)./...
          (gxb.*hfXB+fapp.*hfXB+gxb.*gapp+hb.*fapp+hb.*gapp+gxb.*fapp);
        SSXBpostr = fapp.*hfXB./(gxb.*hfXB+fapp.*hfXB+gxb.*gapp+hb.*fapp+hb.*gapp+gxb.*fapp);

        % normalization for scaling active and passive force (maximal force)
        Fnordv = kxb.*x_0.*SSXBpostr;

        % Calculate Forces (active, passive, preload, afterload)
        force = kxb.*SOVFThick.*(xXBpostr.*XBpostr+xXBprer.*XBprer);
        active = force./Fnordv;
        ppforce_t = sign(SL-SLrest).*PCon_t.*(exp(PExp_t.*abs(SL-SLrest))-1);
        if (SL-SL_c) < 0 
                ppforce_c = 0.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
        elseif (SL-SL_c) == 0 
            ppforce_c = 1./2.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
        else
            ppforce_c = 1.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
        end
        ppforce = ppforce_t + ppforce_c;
        preload = sign(SLset-SLrest).*PCon_t.*(exp(PExp_t.*abs(SLset-SLrest))-1);
        afterload = zeros(size(SL));%KSE./100.*(SLset-SL);

        %% change in SL and forces
        lmbda = SL./SLset;

    %     Cp = 0.2;
    %     b_ff = 50.0;
    %  
    %     passive = Cp.*b_ff.*(-0.5 + 0.5.*(lmbda.*lmbda)).*exp(b_ff.*((-0.5 +0.5.*(lmbda.*lmbda)).*(-0.5 + 0.5.*(lmbda.*lmbda))));
    %  
        dintf = (-ppforce+preload-active+afterload);
    %     
    % %     C1 = -1;
    % %     x_shift = C1 .* (SL(1:end-2) + SL(3:end));
    % %     x_shift = [x_shift(1) ;x_shift; x_shift(end)];
    % %     b = SLmax * 1.5;
    % %     force_shift = (1.5 .* (SL - x_shift)) - b;
    %  
    %  
    %     total_force = -preload + active + passive;% + force_shift;
    %     dSL1 = (-total_force + afterload)/visc;
    % 
    % %     % internal shortening
    % %     C2 = -1;
    % %     dSL = zeros(size(dSL1));
    % %  
    % %     dSL(2:end-1) = C2 .* (dSL1(1:end-2) + dSL1(3:end) - 2.*dSL1(2:end-1));
    % %     dSL(1) = C2 .* (dSL1(2) - dSL1(1));
    % %     dSL(end) = C2 .* (dSL1(end-1) - dSL1(end));
    % 
    %     dSL = dSL1;


        sigma = 6;
        if t<=640
            Gauss = 640;
            GaussCurve = 1./(sqrt(3.55.*pi)) .* (-1/2.*(2*(t-(Gauss))./sigma)*1./sigma) .* exp(-1/2.*((t-(Gauss))./sigma).^2);
        elseif t>640 && t<=840
            GaussCurve = 0;
        else
            Gauss = 840;
            GaussCurve = 1./(sqrt(3.55.*pi)) .* (-1/2.*(2*(t-(Gauss))./sigma)*1./sigma) .* exp(-1/2.*((t-(Gauss))./sigma).^2);
        end

        dSL = 0;% dSL + GaussCurve;

        %% change to Rice model
        sigman = sigman.*exp(konFac2*(lmbda-1));
        sigmap = sigmap.*exp(koffFac2*(lmbda-1));
        gxbmd   = heaviside(x_0-xXBpostr).*exp(sigmap.*((x_0-xXBpostr)./x_0).^2)+...
          (1-heaviside(x_0-xXBpostr)).*exp(sigman.*(((xXBpostr-x_0)./x_0).^2));

        gxbT    = gxb.*gxbmd.*xbmodsp.*Qgxb.^((Temp-310)./10);

        %%
        % Regulation and corssbridge cycling state derivatives
        dTRPNCaL  = konT.*(exp(k_on*(lmbda-1))).*CaiMech.*(1-TRPNCaL) - koffLT.*(exp(k_off*(lmbda-1))^(-1)).*TRPNCaL;
        dTRPNCaH  = konT.*(exp(k_on*(lmbda-1))).*CaiMech.*(1-TRPNCaH) - koffHT.*(exp(k_off*(lmbda-1))^(-1)).*TRPNCaH;
        %%

        dN_NoXB   = -kn_pT.*N_NoXB+ kp_nT.*P_NoXB;
        dP_NoXB   = -kp_nT.*P_NoXB + kn_pT.*N_NoXB;

        dN        = -kn_pT.*N+ kp_nT.*P;
        % dP      = -kp_nT.*P + kn_pT.*N - fappT.*P + gappT.*XBprer + gxbT.*XBpostr;
        dXBprer   = fappT.*P - gappT.*XBprer - hfT.*XBprer + hbT.*XBpostr;
        dXBpostr  = hfT.*XBprer - hbT.*XBpostr - gxbT.*XBpostr;

        dP        = -(dN+dXBprer+dXBpostr);

        % Mean strain of strongly-bound states due to SL motion and XB cycling
        dutyprer  = (hbT.*fappT+gxbT.*fappT)./...    % duty fractions using the
          (fappT.*hfT+gxbT.*hfT+gxbT.*gappT+hbT.*fappT+hbT.*gappT+gxbT.*fappT);
        dutypostr = fappT.*hfT./...                 % King-Alman Rule    
          (fappT.*hfT+gxbT.*hfT+gxbT.*gappT+hbT.*fappT+hbT.*gappT+gxbT.*fappT);
        dxXBprer = 0.5.*dSL+xPsi./dutyprer.*(-xXBprer.*fappT+(xXBpostr-x_0-xXBprer).*hbT);
        dxXBpostr = 0.5.*dSL+ xPsi./dutypostr.*(x_0+xXBprer-xXBpostr).*hfT;

        % Ca buffering by low-affinity troponin C (LTRPNCa)
        FrSBXB    = (XBpostr+XBprer)./(SSXBpostr + SSXBprer);
        dFrSBXB   = (dXBpostr+dXBprer)./(SSXBpostr + SSXBprer);

        if (len_thick-SL) < 0 
            dsovr_ze  = -dSL./2.*0;
        elseif (len_thick-SL) == 0 
            dsovr_ze  = -dSL./2.*1./2;
        else
            dsovr_ze  = -dSL./2.*1;
        end

        if ((2.*len_thin-SL)-len_hbare) < 0
            dsovr_cle = -dSL./2.*0;
        elseif ((2.*len_thin-SL)-len_hbare) == 0
            dsovr_cle = -dSL./2.*1./2;
        else
            dsovr_cle = -dSL./2.*1;
        end

        dlen_sovr = dsovr_ze-dsovr_cle;
        dSOVFThin = dlen_sovr./len_thin;
        dSOVFThick= 2.*dlen_sovr./(len_thick-len_hbare);

        TropTot = Trop_conc.*((1-SOVFThin).*TRPNCaL + ...
          SOVFThin.*(FrSBXB.*TRPNCaH+(1-FrSBXB).*TRPNCaL));
        dTropTot= (Trop_conc.*(-dSOVFThin.*TRPNCaL+(1-SOVFThin).*dTRPNCaL + ...
          dSOVFThin.*(FrSBXB.*TRPNCaH+(1-FrSBXB).*TRPNCaL) + ...
          SOVFThin.*(dFrSBXB.*TRPNCaH+FrSBXB.*dTRPNCaH-dFrSBXB.*TRPNCaL+...
          (1-FrSBXB).*dTRPNCaL)));


        dforce = kxb.*dSOVFThick.*(xXBpostr.*XBpostr+xXBprer.*XBprer) + ...
          kxb.*SOVFThick.*(dxXBpostr.*XBpostr+xXBpostr.*dXBpostr + ...
          dxXBprer.*XBprer+xXBprer.*dXBprer);

        dactive = 0.5.*dforce./Fnordv;
        dppforce_t = sign(SL-SLrest).*PCon_t.*PExp_t.*dSL.*exp(PExp_t.*abs(SL-SLrest));

        if (SL-SL_c) < 0
            dppforce_c = 0.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
        elseif (SL-SL_c) == 0
            dppforce_c = 1./2.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
        else
            dppforce_c = 1.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
        end

        dppforce = dppforce_t + dppforce_c;
        dsfib = dppforce+dactive;

        %%%%%%%%%%%%%%%%%%%%%

        %% EP Model Parameters
        % Constants
        R = 8314;       % [J./kmol.*K]
        Frdy = 96485;   % [C./mol]
        Temp = 310;     % [K]
        FoRT = Frdy./R./Temp;
        Cmem = 1.3810e-10;   % [F] membrane capacitance
        Qpow = (Temp-310)./10;

        % Cell geometry
        cellLength = 100;     % cell length [um]
        cellRadius = 10.25;   % cell radius [um]
        Vcell = pi.*cellRadius.^2.*cellLength.*1e-15;    % [L]
        Vmyo = 0.65.*Vcell; 
        Vsr = 0.035.*Vcell; 
        Vsl = 0.02.*Vcell;
        Vjunc = 0.0539.*.01.*Vcell;

        J_ca_juncsl = 1./1.2134e12; % [L./msec] = 8.2413e-13
        J_ca_slmyo = 1./2.68510e11; % [L./msec] = 3.2743e-12
        J_na_juncsl = 1./(1.6382e12./3.*100); % [L./msec] = 6.1043e-13
        J_na_slmyo = 1./(1.8308e10./3.*100);  % [L./msec] = 5.4621e-11

        % Fractional currents in compartments
        Fjunc = 0.11;   Fsl = 1-Fjunc;
        Fjunc_CaL = 0.9; Fsl_CaL = 1-Fjunc_CaL;

        % Fixed ion concentrations
        Cli = 15;   % Intracellular Cl  [mM]
        Clo = 150;  % Extracellular Cl  [mM]
        Ko  = 5.4;   % Extracellular K   [mM]
        Kio = 135; 
        Nao = 140;  % Extracellular Na  [mM]
        Cao = 2;  % Extracellular Ca  [mM]
        Mgi = 1;    % Intracellular Mg  [mM]

        % Nernst Potentials
        ena_junc = (1./FoRT).*log(Nao./Naj);     % [mV]
        ena_sl = (1./FoRT).*log(Nao./Nasl);       % [mV]
        ek = (1./FoRT).*log(Ko./Kio);	        % [mV]
        eca_junc = (1./FoRT./2).*log(Cao./Caj);   % [mV]
        eca_sl = (1./FoRT./2).*log(Cao./CaSL);     % [mV]
        ecl = (1./FoRT).*log(Cli./Clo);            % [mV]

        % Na transport parameters

        GNa=16;
        GNaB = 0.297e-3;    % [mS./uF]
        IbarNaK = 1.90719;     % [uA./uF]
        KmNaip = 11;         % [mM]
        KmKo = 1.5;         % [mM]

        %% K current parameters
        pNaK = 0.01833;
        GtoSlow = 0.06.*1;     % [mS./uF] %0.09 CaMKII
        GtoFast = 0.02.*1;     % [mS./uF]
        gkp = 0.001;

        % Cl current parameters
        GClCa = 0.109625;   % [mS./uF]
        GClB = 9e-3;        % [mS./uF]
        KdClCa = 100e-3;    % [mM]

        % I_Ca parameters
        pNa = 1.5e-8;       % [cm./sec]
        pCa = 5.4e-4;       % [cm./sec]
        pK = 2.7e-7;        % [cm./sec]
        Q10CaL = 1.8;

        % Ca transport parameters
        IbarNCX = 9.0;      % [uA./uF]
        KmCai = 3.59e-3;    % [mM]
        KmCao = 1.3;        % [mM]
        KmNai = 12.29;      % [mM]
        KmNao = 87.5;       % [mM]
        ksat = 0.27;        % [none]
        nu = 0.35;          % [none]
        Kdact = 0.256e-3;   % [mM]
        Q10NCX = 1.57;      % [none]
        IbarSLCaP = 0.0673; % [uA./uF](2.2 umol./L cytosol./sec)
        KmPCa = 0.5e-3;     % [mM]
        GCaB = 2.513e-4;    % [uA./uF]
        Q10SLCaP = 2.35;    % [none]

        % SR flux parameters
        Q10SRCaP = 2.6;          % [none]
        Vmax_SRCaP = 5.3114e-3;  % [mM./msec] (286 umol./L cytosol./sec)
        Kmf = 0.246e-3;          % [mM]
        Kmr = 1.7;               % [mM]L cytosol
        hillSRCaP = 1.787;       % [mM]
        ks = 25;                 % [1./ms]
        koCa = 10;               % [mM.^-2 1./ms]   %default 10   modified 20
        kom = 0.06;              % [1./ms]
        kiCa = 0.5;              % [1./mM./ms]
        kim = 0.005;             % [1./ms]
        ec50SR = 0.45;           % [mM]

        % Buffering parameters
        % Note: we are using [1./ms] and [1./mM./ms], which differs from that in the paper
        % koff: [1./s] = 1e-3.*[1./ms];  kon: [1./uM./s] = [1./mM./ms]
        Bmax_Naj = 7.561;       % [mM]  % Na buffering
        Bmax_Nasl = 1.65;       % [mM]
        koff_na = 1e-3;         % [1./ms]
        kon_na = 0.1e-3;        % [1./mM./ms]
        Bmax_TnClow = 70e-3;    % [mM]                      % TnC low affinity
        koff_tncl = 19.6e-3;    % [1./ms]
        kon_tncl = 32.7;        % [1./mM./ms]
        Bmax_TnChigh = 140e-3;  % [mM]                      % TnC high affinity
        koff_tnchca = 0.032e-3; % [1./ms]
        kon_tnchca = 2.37;      % [1./mM./ms]
        koff_tnchmg = 3.33e-3;  % [1./ms]
        kon_tnchmg = 3e-3;      % [1./mM./ms]
        Bmax_CaM = 24e-3;       % [mM]  % CaM buffering
        koff_cam = 238e-3;      % [1./ms]
        kon_cam = 34;           % [1./mM./ms]
        Bmax_myosin = 140e-3;   % [mM]                      % Myosin buffering
        koff_myoca = 0.46e-3;   % [1./ms]
        kon_myoca = 13.8;       % [1./mM./ms]
        koff_myomg = 0.057e-3;  % [1./ms]
        kon_myomg = 0.0157;     % [1./mM./ms]
        Bmax_SR = 19.*.9e-3;     % [mM] (Bers text says 47e-3) 19e-3
        koff_sr = 60e-3;        % [1./ms]
        kon_sr = 100;           % [1./mM./ms]
        Bmax_SLlowsl = 37.4e-3.*Vmyo./Vsl;        % [mM]    % SL buffering
        Bmax_SLlowj = 4.6e-3.*Vmyo./Vjunc.*0.1;    % [mM]
        koff_sll = 1300e-3;     % [1./ms]
        kon_sll = 100;          % [1./mM./ms]
        Bmax_SLhighsl = 13.4e-3.*Vmyo./Vsl;       % [mM]
        Bmax_SLhighj = 1.65e-3.*Vmyo./Vjunc.*0.1;  % [mM]
        koff_slh = 30e-3;       % [1./ms]
        kon_slh = 100;          % [1./mM./ms]
        Bmax_Csqn = 140e-3.*Vmyo./Vsr;            % [mM] % Bmax_Csqn = 2.6;      % Csqn buffering
        koff_csqn = 65;         % [1./ms]
        kon_csqn = 100;         % [1./mM./ms]


        %% Membrane Currents
        % I_Na: Fast Na Current
        am = 0.32.*(Vm+47.13)./(1-exp(-0.1.*(Vm+47.13)));
        bm = 0.08.*exp(-Vm./11);
        if Vm >= -40
            ah = 0; aj = 0;
            bh = 1./(0.13.*(1+exp(-(Vm+10.66)./11.1)));
            bj = 0.3.*exp(-2.535e-7.*Vm)./(1+exp(-0.1.*(Vm+32)));
        else
            ah = 0.135.*exp((80+Vm)./-6.8);
            bh = 3.56.*exp(0.079.*Vm)+3.1e5.*exp(0.35.*Vm);
            aj = (-1.2714e5.*exp(0.2444.*Vm)-3.474e-5.*exp(-0.04391.*Vm)).*(Vm+37.78)./(1+exp(0.311.*(Vm+79.23)));
            bj = 0.1212.*exp(-0.01052.*Vm)./(1+exp(-0.1378.*(Vm+40.14)));
        end
        dm = am.*(1-m)-bm.*m;
        dh = ah.*(1-h)-bh.*h;
        dj = aj.*(1-j)-bj.*j;

        I_Na_junc = Fjunc.*GNa.*m.^3.*h.*j.*(Vm-ena_junc);
        I_Na_sl = Fsl.*GNa.*m.^3.*h.*j.*(Vm-ena_sl);
        I_Na = I_Na_junc+I_Na_sl;

        % I_nabk: Na Background Current
        I_nabk_junc = Fjunc.*GNaB.*(Vm-ena_junc);
        I_nabk_sl = Fsl.*GNaB.*(Vm-ena_sl);
        I_nabk = I_nabk_junc+I_nabk_sl;

        % I_nak: Na./K Pump Current
        sigma = (exp(Nao./67.3)-1)./7;
        fnak = 1./(1+0.1245.*exp(-0.1.*Vm.*FoRT)+0.0365.*sigma.*exp(-Vm.*FoRT));
        I_nak_junc = Fjunc.*IbarNaK.*fnak.*Ko ./(1+(KmNaip./Naj).^4) ./(Ko+KmKo);
        I_nak_sl = Fsl.*IbarNaK.*fnak.*Ko ./(1+(KmNaip./Nasl).^4) ./(Ko+KmKo);
        I_nak = I_nak_junc+I_nak_sl;

        % I_kr: Rapidly Activating K Current
        gkr = 0.03.*sqrt(Ko./5.4);
        xrss = 1./(1+exp(-(Vm+50)./7.5));
        tauxr = 1./(1.38e-3.*(Vm+7)./(1-exp(-0.123.*(Vm+7)))+6.1e-4.*(Vm+10)./(exp(0.145.*(Vm+10))-1));
        dxkr = (xrss-xkr)./tauxr;
        rkr = 1./(1+exp((Vm+33)./22.4));
        I_kr = gkr.*xkr.*rkr.*(Vm-ek);

        % I_ks: Slowly Activating K Current
        pcaks_junc = -log10(Caj)+3.0;
        pcaks_sl = -log10(CaSL)+3.0;
        gks_junc = 0.07.*(0.057 +0.19./(1+ exp((-7.2+pcaks_junc)./0.6)));
        gks_sl = 0.07.*(0.057 +0.19./(1+ exp((-7.2+pcaks_sl)./0.6)));
        eks = (1./FoRT).*log((Ko+pNaK.*Nao)./(Kio+pNaK.*Nai));
        xsss = 1./(1+exp(-(Vm-1.5)./16.7));
        tauxs = 1./(7.19e-5.*(Vm+30)./(1-exp(-0.148.*(Vm+30)))+1.31e-4.*(Vm+30)./(exp(0.0687.*(Vm+30))-1));
        dxks = (xsss-xks)./tauxs;
        I_ks_junc = Fjunc.*gks_junc.*xks.^2.*(Vm-eks);
        I_ks_sl = Fsl.*gks_sl.*xks.^2.*(Vm-eks);
        I_ks = I_ks_junc+I_ks_sl;

        %I_kp: Plateau K current
        kp_kp = 1./(1+exp(7.488-Vm./5.98));
        I_kp_junc = Fjunc.*gkp.*kp_kp.*(Vm-ek);
        I_kp_sl = Fsl.*gkp.*kp_kp.*(Vm-ek);
        I_kp = I_kp_junc+I_kp_sl;

        %% I_to: Transient Outward K Current (slow and fast components)
        xtoss = 1./(1+exp(-(Vm+3.0)./15));
        ytoss = 1./(1+exp((Vm+33.5)./10));
        rtoss = 1./(1+exp((Vm+33.5)./10));
        tauxtos = 9./(1+exp((Vm+3.0)./15))+0.5;
        tauytos = 3e3./(1+exp((Vm+60.0)./10))+30;
        taurtos = 2.8e3./(1+exp((Vm+60.0)./10))+220; 
        dxtos = (xtoss-xtos)./tauxtos;
        dytos = (ytoss-ytos)./tauytos;
        drtos= (rtoss-rtos)./taurtos; 
        I_tos = GtoSlow.*xtos.*(ytos+0.5.*rtos).*(Vm-ek);    % [uA./uF]

        tauxtof = 3.5.*exp(-Vm.*Vm./30./30)+1.5;
        tauytof = 20.0./(1+exp((Vm+33.5)./10))+20.0;
        dxtof = (xtoss-xtof)./tauxtof;
        dytof = (ytoss-ytof)./tauytof;
        I_tof = GtoFast.*xtof.*ytof.*(Vm-ek);
        I_to = I_tos + I_tof;

        % I_ki: Time-Independent K Current
        aki = 1.02./(1+exp(0.2385.*(Vm-ek-59.215)));
        bki =(0.49124.*exp(0.08032.*(Vm+5.476-ek)) + exp(0.06175.*(Vm-ek-594.31))) ./(1 + exp(-0.5143.*(Vm-ek+4.753)));
        kiss = aki./(aki+bki);
        I_ki = 0.9.*sqrt(Ko./5.4).*kiss.*(Vm-ek);

        % I_ClCa: Ca-activated Cl Current, I_Clbk: background Cl Current
        I_ClCa_junc = Fjunc.*GClCa./(1+KdClCa./Caj).*(Vm-ecl);
        I_ClCa_sl = Fsl.*GClCa./(1+KdClCa./CaSL).*(Vm-ecl);
        I_ClCa = I_ClCa_junc+I_ClCa_sl;
        I_Clbk = GClB.*(Vm-ecl);

        %% I_Ca: L-type Calcium Current
        dss = 1./(1+exp(-(Vm+14.5)./6.0));
        taud = dss.*(1-exp(-(Vm+14.5)./6.0))./(0.035.*(Vm+14.5));
        fss = 1./(1+exp((Vm+35.06)./3.6))+0.6./(1+exp((50-Vm)./20));
        tauf = 1./(0.0197.*exp( -(0.0337.*(Vm+14.5)).^2 )+0.02);
        dd = (dss-d)./taud;
        df = (fss-f)./tauf;
        dfcaBj = 1.7.*Caj.*(1-fcaBj)-11.9e-3.*fcaBj; % fCa_junc
        dfcaBsl = 1.7.*CaSL.*(1-fcaBsl)-11.9e-3.*fcaBsl; % fCa_sl
        fcaCaMSL=0;
        fcaCaj= 0;
        ibarca_j = pCa.*4.*(Vm.*Frdy.*FoRT) .* (0.341.*Caj.*exp(2.*Vm.*FoRT)-0.341.*Cao) ./(exp(2.*Vm.*FoRT)-1);
        ibarca_sl = pCa.*4.*(Vm.*Frdy.*FoRT) .* (0.341.*CaSL.*exp(2.*Vm.*FoRT)-0.341.*Cao) ./(exp(2.*Vm.*FoRT)-1);
        ibark = pK.*(Vm.*Frdy.*FoRT).*(0.75.*Kio.*exp(Vm.*FoRT)-0.75.*Ko) ./(exp(Vm.*FoRT)-1);
        ibarna_j = pNa.*(Vm.*Frdy.*FoRT) .*(0.75.*Naj.*exp(Vm.*FoRT)-0.75.*Nao)  ./(exp(Vm.*FoRT)-1);
        ibarna_sl = pNa.*(Vm.*Frdy.*FoRT) .*(0.75.*Nasl.*exp(Vm.*FoRT)-0.75.*Nao)  ./(exp(Vm.*FoRT)-1);
        I_Ca_junc = (Fjunc_CaL.*ibarca_j.*d.*f.*((1-fcaBj)+fcaCaj).*Q10CaL.^Qpow).*0.45.*1;
        I_Ca_sl = (Fsl_CaL.*ibarca_sl.*d.*f.*((1-fcaBsl)+fcaCaMSL).*Q10CaL.^Qpow).*0.45.*1;
        I_Ca = I_Ca_junc+I_Ca_sl;
        I_CaK = (ibark.*d.*f.*(Fjunc_CaL.*(fcaCaj+(1-fcaBj))+Fsl_CaL.*(fcaCaMSL+(1-fcaBsl))).*Q10CaL.^Qpow).*0.45.*1;
        I_CaNa_junc = (Fjunc_CaL.*ibarna_j.*d.*f.*((1-fcaBj)+fcaCaj).*Q10CaL.^Qpow).*0.45.*1;
        I_CaNa_sl = (Fsl_CaL.*ibarna_sl.*d.*f.*((1-fcaBsl)+fcaCaMSL).*Q10CaL.^Qpow).*.45.*1;
        I_CaNa = I_CaNa_junc+I_CaNa_sl;
        I_Catot = I_Ca+I_CaK+I_CaNa;

        % I_ncx: Na./Ca Exchanger flux
        Ka_junc = 1./(1+(Kdact./Caj).^3);
        Ka_sl = 1./(1+(Kdact./CaSL).^3);
        s1_junc = exp(nu.*Vm.*FoRT).*Naj.^3.*Cao;
        s1_sl = exp(nu.*Vm.*FoRT).*Nasl.^3.*Cao;
        s2_junc = exp((nu-1).*Vm.*FoRT).*Nao.^3.*Caj;
        s3_junc = KmCai.*Nao.^3.*(1+(Naj./KmNai).^3) + KmNao.^3.*Caj.*(1+Caj./KmCai)+KmCao.*Naj.^3+Naj.^3.*Cao+Nao.^3.*Caj;
        s2_sl = exp((nu-1).*Vm.*FoRT).*Nao.^3.*CaSL;
        s3_sl = KmCai.*Nao.^3.*(1+(Nasl./KmNai).^3) + KmNao.^3.*CaSL.*(1+CaSL./KmCai)+KmCao.*Nasl.^3+Nasl.^3.*Cao+Nao.^3.*CaSL;
        I_ncx_junc = Fjunc.*IbarNCX.*Q10NCX.^Qpow.*Ka_junc.*(s1_junc-s2_junc)./s3_junc./(1+ksat.*exp((nu-1).*Vm.*FoRT));
        I_ncx_sl = Fsl.*IbarNCX.*Q10NCX.^Qpow.*Ka_sl.*(s1_sl-s2_sl)./s3_sl./(1+ksat.*exp((nu-1).*Vm.*FoRT));
        I_ncx = I_ncx_junc+I_ncx_sl;

        % I_pca: Sarcolemmal Ca Pump Current
        I_pca_junc = Fjunc.*Q10SLCaP.^Qpow.*IbarSLCaP.*Caj.^1.6./(KmPCa.^1.6+Caj.^1.6);
        I_pca_sl = Fsl.*Q10SLCaP.^Qpow.*IbarSLCaP.*CaSL.^1.6./(KmPCa.^1.6+CaSL.^1.6);
        I_pca = I_pca_junc+I_pca_sl;

        % I_cabk: Ca Background Current
        I_cabk_junc = Fjunc.*GCaB.*(Vm-eca_junc);
        I_cabk_sl = Fsl.*GCaB.*(Vm-eca_sl);
        I_cabk = I_cabk_junc+I_cabk_sl;

        %% SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
        MaxSR = 15; MinSR = 1;
        kCaSR = MaxSR - (MaxSR-MinSR)./(1+(ec50SR./CaSR).^2.5);
        koSRCa = koCa./kCaSR;
        kiSRCa = kiCa.*kCaSR;
        RI = 1-RyRr-RyRo-RyRi;
        dRyRr = (kim.*RI-kiSRCa.*Caj.*RyRr)-(koSRCa.*Caj.^2.*RyRr-kom.*RyRo);      % R
        dRyRo = (koSRCa.*Caj.^2.*RyRr-kom.*RyRo)-(kiSRCa.*Caj.*RyRo-kim.*RyRi);    % O
        dRyRi = (kiSRCa.*Caj.*RyRo-kim.*RyRi)-(kom.*RyRi-koSRCa.*Caj.^2.*RI);      % I

        %%
        J_SRCarel = ks.*RyRo.*(CaSR-Caj);          % [mM./ms]

        J_serca = Q10SRCaP.^Qpow.*Vmax_SRCaP.*((Cai./Kmf).^hillSRCaP-(CaSR./Kmr).^hillSRCaP)...
            ./(1+(Cai./Kmf).^hillSRCaP+(CaSR./Kmr).^hillSRCaP);
        J_SRleak = 5.348e-6.*(CaSR-Caj);           %   [mM./ms]

        %% Sodium and Calcium Buffering
        dNaBj = kon_na.*Naj.*(Bmax_Naj-NaBj)-koff_na.*NaBj;        % NaBj      [mM./ms]
        dNaBsl = kon_na.*Nasl.*(Bmax_Nasl-NaBsl)-koff_na.*NaBsl;       % NaBsl     [mM./ms]

        % Cytosolic Ca Buffers
    %     dTnCL = kon_tncl.*Cai.*(Bmax_TnClow-TnCL)-koff_tncl.*TnCL;
        dTnCL = (Bmax_TnClow.*(dTropTot)./Trop_conc);            % TnCL      [mM./ms]
        dTnCHc = kon_tnchca.*Cai.*(Bmax_TnChigh-TnCHc-TnCHm)-koff_tnchca.*TnCHc; % TnCHc     [mM./ms]
        dTnCHm = kon_tnchmg.*Mgi.*(Bmax_TnChigh-TnCHc-TnCHm)-koff_tnchmg.*TnCHm;   % TnCHm     [mM./ms]

        dCaM = kon_cam.*Cai.*(Bmax_CaM-CaM)-koff_cam.*CaM;                 % CaM       [mM./ms]
        dMyoc = kon_myoca.*Cai.*(Bmax_myosin-Myoc-Myom)-koff_myoca.*Myoc;    % Myosin_ca [mM./ms]
        dMyom = kon_myomg.*Mgi.*(Bmax_myosin-Myoc-Myom)-koff_myomg.*Myom;      % Myosin_mg [mM./ms]
        dSRB = kon_sr.*Cai.*(Bmax_SR-SRB)-koff_sr.*SRB;                    % SRB       [mM./ms]
        J_CaB_cytosol = dCaM + dMyoc + dMyom + dSRB;
        J_CaB_TnC = dTnCHc + dTnCL + dTnCHm;

        % Junctional and SL Ca Buffers
        dSLLj = kon_sll.*Caj.*(Bmax_SLlowj-SLLj)-koff_sll.*SLLj;       % SLLj      [mM./ms]
        dSLLsl = kon_sll.*CaSL.*(Bmax_SLlowsl-SLLsl)-koff_sll.*SLLsl;      % SLLsl     [mM./ms]
        dSLHj = kon_slh.*Caj.*(Bmax_SLhighj-SLHj)-koff_slh.*SLHj;      % SLHj      [mM./ms]
        dSLHsl = kon_slh.*CaSL.*(Bmax_SLhighsl-SLHsl)-koff_slh.*SLHsl;     % SLHsl     [mM./ms]
        J_CaB_junction = dSLLj+dSLHj;
        J_CaB_sl = dSLLsl+dSLHsl;

        %% Ion concentrations
        % SR Ca Concentrations
        dCsqnb = kon_csqn.*CaSR.*(Bmax_Csqn-Csqnb)-koff_csqn.*Csqnb;       % Csqn      [mM./ms]

        % Sodium Concentrations
        I_Na_tot_junc = I_Na_junc+I_nabk_junc+3.*I_ncx_junc+3.*I_nak_junc+I_CaNa_junc;   % [uA./uF]
        I_Na_tot_sl = I_Na_sl+I_nabk_sl+3.*I_ncx_sl+3.*I_nak_sl+I_CaNa_sl;   % [uA./uF]

        dNaj = -I_Na_tot_junc.*Cmem./(Vjunc.*Frdy)+J_na_juncsl./Vjunc.*(Nasl-Naj)-dNaBj;
        dNasl = -I_Na_tot_sl.*Cmem./(Vsl.*Frdy)+J_na_juncsl./Vsl.*(Naj-Nasl)...
            +J_na_slmyo./Vsl.*(Nai-Nasl)-dNaBsl;
        dNai = J_na_slmyo./Vmyo.*(Nasl-Nai);             % [mM./msec]

        % Potassium Concentration
        I_K_tot = I_to+I_kr+I_ks+I_ki-2.*I_nak+I_CaK+I_kp;     % [uA./uF]

        % Calcium Concentrations
        I_Ca_tot_junc = I_Ca_junc+I_cabk_junc+I_pca_junc-2.*I_ncx_junc;                   % [uA./uF]
        I_Ca_tot_sl = I_Ca_sl+I_cabk_sl+I_pca_sl-2.*I_ncx_sl;            % [uA./uF]


        %% Calcium concentrations

        dCaSR = J_serca ...
            - (J_SRleak.*Vmyo./Vsr+J_SRCarel)...
            - dCsqnb ;         % Ca_sr     [mM./ms] %Ratio 3 leak current

        dCaj = - I_Ca_tot_junc.*Cmem./(Vjunc.*2.*Frdy)...
            + J_ca_juncsl./Vjunc.*(CaSL-Caj)...
            - J_CaB_junction ...
            + (J_SRCarel).*Vsr./Vjunc ...
            + J_SRleak.*Vmyo./Vjunc;  % Ca_j

        dCaSL = -I_Ca_tot_sl.*Cmem./(Vsl.*2.*Frdy) ...
            + J_ca_juncsl./Vsl.*(Caj-CaSL)...
            + J_ca_slmyo./Vsl.*(Cai-CaSL) ...
            - J_CaB_sl;   % Ca_sl

        dCai = -J_serca.*Vsr./Vmyo ...
            - J_CaB_cytosol ...
            + J_ca_slmyo./Vmyo.*(CaSL-Cai)...
            - J_CaB_TnC;  % [mM./msec]


        %% Simulation type
        I_app = 0.0;

        if t>=10 && t<=15
            I_app = 9.5;
        elseif t>=610 && t<=615
            I_app = 9.5;
        else
            I_app = 0.0;
        end

        %% Membrane Potential
        I_Na_tot = I_Na_tot_junc + I_Na_tot_sl;          % [uA./uF]
        I_Cl_tot = I_ClCa+I_Clbk;                        % [uA./uF]
        I_Ca_tot = I_Ca_tot_junc+I_Ca_tot_sl;
        I_tot = I_Na_tot+I_Cl_tot+I_Ca_tot+I_K_tot;
        dVm = -(I_tot-I_app);

         %% output vector
        ydot(1)             =  dVm;
        ydot(2)      =  dm;
        ydot(3)      =  dh;
        ydot(4)      =  dj;
        ydot(5)      =  dd;
        ydot(6)      =  df;
        ydot(7)      =  dfcaBj;
        ydot(8)      =  dfcaBsl;
        ydot(9)      =  dxtos;
        ydot(10)     =  dytos;
        ydot(11)     =  dxtof;
        ydot(12)     =  dytof;
        ydot(13)     =  dxkr;
        ydot(14)     =  dxks;
        ydot(15)     =  dRyRr;
        ydot(16)     =  dRyRo;
        ydot(17)     =  dRyRi;
        ydot(18)     =  dNaBj;
        ydot(19)     =  dNaBsl;
        ydot(20)     =  dTnCL;
        ydot(21)     =  dTnCHc;
        ydot(22)     =  dTnCHm;
        ydot(23)     =  dCaM;
        ydot(24)     =  dMyoc;
        ydot(25)     =  dMyom;
        ydot(26)     =  dSRB;
        ydot(27)     =  dSLLj;
        ydot(28)     =  dSLLsl;
        ydot(29)     =  dSLHj;
        ydot(30)     =  dSLHsl;
        ydot(31)     =  dCsqnb;
        ydot(32)     =  dCaSR;
        ydot(33)     =  dNaj;
        ydot(34)     =  dNasl;
        ydot(35)     =  dNai;
        ydot(36)     =  dCaj;
        ydot(37)     =  dCaSL;
        ydot(38)     =  dCai;
        ydot(39)     =  drtos;

        %% mechanics
        ydot(40)     = dN_NoXB;
        ydot(41)     = dP_NoXB;
        ydot(42)     = dN;
        ydot(43)     = dP;
        ydot(44)     = dXBprer;
        ydot(45)     = dXBpostr;
        ydot(46)     = dSL;
        ydot(47)     = dxXBpostr;
        ydot(48)     = dxXBprer;
        ydot(49)     = dTRPNCaL;
        ydot(50)     = dTRPNCaH;
        ydot(51)     = dforce;
        ydot(52)     = dintf;


        if isreal(y) == 0
            ydot = zeros(size(ydot));
        end

        if isreal(ydot) == 0
            ydot = zeros(size(ydot));
        end


        % ----- END EC COUPLING MODEL ---------------
        % adjust output depending on the function call

        if (nargin == 10)
            output = ydot;
        elseif (nargin == 11) & strcmp(runType,'ydot')
            output = ydot;
        elseif (nargin == 11) & strcmp(runType,'rates')
            output = r;
        elseif (nargin == 11) & strcmp(runType,'currents')
            %currents = [I_Na I_nabk I_nak I_kr I_ks I_kp I_tos I_tof I_ki I_ClCa I_Clbk I_Catot I_ncx I_pca I_cabk J_serca.*Vmyo./Vsr];
            %currents = [I_Na I_tof I_tos I_kr I_ks I_ClCa I_Catot J_SRCarel.*Vsr./Vmyo J_SRleak RI I_ncx];
            currents= [];
            output = currents;
        end

    end
end