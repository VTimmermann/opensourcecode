function RunGrandiRice

%% Initial conditions
mo=1.405627e-3;
ho= 9.867005e-1;
jo=9.915620e-1; 
do=7.175662e-6; 
fo=1.000681; 
fcaBjo=2.421991e-2;
fcaBslo=1.452605e-2;
xtoso=4.051574e-3;
ytoso=9.945511e-1; 
xtofo=4.051574e-3; 
ytofo= 9.945511e-1; 
xkro=8.641386e-3; 
xkso= 5.412034e-3;
RyRro=8.884332e-1;
RyRoo=8.156628e-7; 
RyRio=1.024274e-7; 
NaBjo=3.539892;
NaBslo=7.720854e-1; 
TnCLo=8.773191e-3; 
TnCHco=1.078283e-1; 
TnCHmo=1.524002e-2; 
CaMo=2.911916e-4; 
Myoco=1.298754e-3; 
Myomo=1.381982e-1;
SRBo=2.143165e-3; 
SLLjo=9.566355e-3; 
SLLslo=1.110363e-1; 
SLHjo=7.347888e-3; 
SLHslo=7.297378e-2; 
Csqnbo= 1.242988;
Ca_sro=0.1e-1; %5.545201e-1; 
Najo=9.06;%8.80329; 
Naslo=9.06;%8.80733; 
Naio=9.06;%8.80853; 
Kio=120; 
Cajo=1.737475e-4; 
Caslo= 1.031812e-4; 
Caio=8.597401e-5; 
Vmo=-8.09763e+1; 
rtoso=0.9946; 
ICajuncinto=1; 
ICaslinto=0;
C1o=0.0015;       % [] 
C2o=0.0244;       % [] 
C3o=0.1494;       % [] 
C4o=0.4071;       % [] 
C5o=0.4161;       % [] 
C7o=0.0001;       % [] 
C8o=0.0006;       % [] 
C9o=0.0008;       % [] 
C10o=0;           % [] 
C11o=0;           % [] 
C12o=0;           % [] 
C13o=0;           % [] 
C14o=0;           % [] 
C15o=0;           % [] 
O1o=0;            % [] 
O2o=0;            % [] 
C6o=1-(C1o+C2o+C3o+C4o+C5o+C7o+C8o+C9o+C10o+C11o+C12o+C13o+C14o+C15o+O1o+O2o);       % []
% UIC3o = PoUIC3;     
% UIC2o = PoUIC2;
% UIFo = PoUIF;
% UIM1o = PoUIM1;
% UC3o = PoUC3;
% UC2o =  PoUC2;
% UC1o = PoUC1; 
% UOo =  PoUO;
% UIM2o = PoUIM2;
% LC3o = PoLC3; 
% LC2o =  PoLC2 ;
% LC1o = PoLC1; 
% LOo =  PoLO;


%%% Rice Model Initial Conditions %%%
N_NoXB = 1.0;
P_NoXB = 0.0;
N = 0.97;

XBprer = 3.0494964880038e-7;
XBpostr = 1.81017564383744e-6;
SL = 1.85;
xXBpostr = 0.00700005394873882;
xXBprer = 3.41212828972468e-8; 
TRPNCaL = 0.0147730085063734;%0.017;%
TRPNCaH = 0.13066096561522;%0.14;%
force = -4.5113452510363e-6;
intf = 0;
curve = 1;
r_KV = 0;
s_KV = 1;

P = 1-N-XBprer-XBpostr;
% mu = 100;

% Gating variables      
%   1       2       3       4       5       6       7       8       9       10      11      12      13
%%   m       h       j       d       f       fcaBj   fcaBsl   xtos    ytos    xtof    ytof    xkr     xks   
%y10=[1.2e-3;0.99;   0.99;   0.0;    1.0;    0.0141; 0.0141;     0;      1;      0.0;    1.0;    0.0;    6e-3;];
y10=[mo; ho; jo; do; fo; fcaBjo; fcaBslo; xtoso; ytoso; xtofo; ytofo; xkro; xkso;];   
% RyR and Buffering variables
%   14      15      16      17      18      19      20      21      22      23      24
%%   RyRr    RyRo    RyRi    NaBj    NaBsl   TnCL    TnCHc   TnCHm   CaM     Myoc    Myom  
y20=[RyRro; RyRoo; RyRio; NaBjo; NaBslo; TnCLo; TnCHco; TnCHmo; CaMo; Myoco; Myomo;];           
%y20=[1;     0;      0;      1.8;   0.8;    0.012;   0.112;  0.01;   0.4e-3; 1.9e-3; 0.135;];
% More buffering variables
%   25      26      27      28      29      30
%%   SRB     SLLj   SLLsl    SLHj    SLHsl  Csqnb
y30=[SRBo; SLLjo; SLLslo; SLHjo; SLHslo; Csqnbo];
%y30=[3.3e-3; 0.012; 0.012; 0.13;  0.13;  1.5;];
%   Intracellular concentrations/ Membrane voltage
%    31      32      33      34      35      36      37     38     39    40   41
%%    Ca_sr   Naj     Nasl    Nai     Ki      Caj    Casl    Cai   Vm  rtos ?
y40=[Ca_sro; Najo; Naslo; Naio; Kio; Cajo; Caslo; Caio; Vmo; rtoso; 1]; 
y50=[C1o; C2o; C3o; C4o; C5o; C6o; C7o; C8o; C9o; C10o; C11o; C12o; C13o; C14o; C15o; O1o];
%y40=[0.9;    8.8;    8.8;    8.8;    135;    0.1e-3; 0.1e-3; 0.1e-3; -88;  0.89; 0;          0;];
% y50=[UIC3o; UIC2o; UIFo; UIM1o; UC3o; UC2o; UC1o; UOo; UIM2o; LC3o; LC2o; LC1o; LOo ];    
%   Intracellular concentrations/ Membrane voltage
%    42      43      44      45      46      47      48     49     50    51
%%    N_NoXB   P_NoXB     N    P     XBprer      XBpostr    SL    xXBpostr   xXBprer  TRPNCaL
y60=[N_NoXB; P_NoXB; N; P; XBprer; XBpostr; SL; xXBpostr; xXBprer; TRPNCaL]; 

%    52      53      54      55      56
%%    TRPNCaH   force     intf    curve     mu 
y70=[TRPNCaH; force; intf;]; 


% y0  = [y10;y20;y30;y40;y50;y60;y70];    
load GrandiRiceOpti.mat

%% Single Run Simulation

CL = 1000;
beats = 200;
tspan = [0:0.1:CL];

load merged.mat

konFac = x(1);
koffFac = x(2);
k_on = x(3);
k_off = x(4);
konFac2 = x(5);
koffFac2 = x(6);
sigman = x(7);
sigmap = x(8);

for i=1:beats
    i
    options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
    [t,y] = ode15s(@GrandiRice,tspan,y0,options,konFac, koffFac,k_on, k_off,konFac2, koffFac2, sigman, sigmap);
    y0=y(end,:);
end

save GR_RefOpti