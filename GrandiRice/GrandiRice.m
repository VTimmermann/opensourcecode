% A novel computational model of the human ventricular action potential and Ca transient.
% Grandi E, Pasqualini FS, Bers DM.
% J Mol Cell Cardiol. 2010 Jan;48(1):112-21. Epub 2009 Oct 14.
% PMID:19835882

function output = GrandiRice(t,y,konFac, koffFac,k_on, k_off,konFac2, koffFac2, sigman, sigmap)

ydot = zeros(size(y));

%% Rice Model
N_NoXB             = y(58);
P_NoXB             = y(59);
N                  = y(60);
P                  = y(61);
XBprer             = y(62);
XBpostr            = y(63);
SL                 = y(64);
xXBpostr           = y(65);
xXBprer            = y(66);
TRPNCaL            = y(67);
TRPNCaH            = y(68);
force              = y(69);
intf               = y(70);

% Sarcomere Geometry
SLmax   = 2.4;        % (um) maximum sarcomere length
SLmin   = 1.4;        % (um) minimum sarcomere length
SLset   = 1.89;
len_thin  = 1.2;      % (um) thin filament length
len_thick = 1.65;     % (um) thick filament length
len_hbare = 0.1;      % (um) length of bare portion of thick filament

% Temperature Dependence
Qkon = 1.5;
Qkoff = 1.3;
Qkn_p = 1.6;
Qkp_n = 1.6;
Qfapp = 6.25;
Qgapp = 2.5;
Qhf = 6.25;
Qhb = 6.25;
Qgxb = 6.25;

% Ca binding to troponin
kon     = 50e-3.*konFac;      % (1./[ms uM])
koffL   = 250e-3.*koffFac;     % (1./ms)
koffH   = 25e-3;      % (1./ms)
perm50  = 0.45;       % perm variable that controls n to p transition
nperm   = 10;         %   in Hill-like fashion
kn_p    = 500e-3;     % (1./ms)
kp_n    = 50e-3;      % (1./ms)
koffmod = 0.9;        % mod to change species

% Thin filament regulation and crossbridge cycling
fapp    = 500e-3;     % (1./ms) XB on rate
gapp    = 70e-3;      % (1./ms) XB off rate
gslmod  = 6;          % controls SL effect on gapp
hfXB    = 2000e-3;    % (1./ms) rate between pre-force and force states

%%
hfmdc   = 5;          % 
hb      = 400e-3;     % (1./ms) rate between pre-force and force states
hbmdc   = 0;          % 
gxb     = 70e-3;      % (1./ms) ATP consuming transition rate
% sigmap  = 8;          % distortion dependence of STP using transition gxb
% sigman  = 1;     % 
xbmodsp = 0.2;        % rabbit specific modification for XB cycling rates

% Mean strain of strongly-bound states
x_0     = 0.007;      % (um) strain induced by head rotation
xPsi    = 2;          % scaling factor balancing SL motion and XB cycling

% Normalized active and passive force
SLrest  = 1.89;       % (um) rest SL length for 0 passive force
PCon_t  = 0.002;      % (norm Force) passive force due to titin
PExp_t  = 10;         %   these apply to trabeculae and single cells only
SL_c    = 2.25;       % (um) resting length for collagen
PCon_c  = 0.02;       % (norm Force) passive force due to collagen
PExp_c  = 70;         %   these apply to trabeculae and single cells only


% Calculation of complete muscle response
massf   = 0.00025e6;  % ([norm Force ms.^2]./um) muscle mass
visc    = 0.003e3;    % ([norm Force ms]./um) muscle viscosity
KSE     = 1;          % (norm Force./um) series elastic element
kxb     = 120;        % (mN./mm.^2) maximal force
Trop_conc = 70;       % (uM) troponin concentration


%% Rice Model
% Time-Varying Parameters
ca50 = 1/30;
CaiMech   = y(38).*1000/ca50;        %cytosolic calcium concentration from EP model
Temp      = 310;            % Temperature (K)

% Compute single overlap fractions
sovr_ze   = min(len_thick./2,SL./2);            % z-line end
sovr_cle  = max(SL./2-(SL-len_thin),len_hbare./2); % centerline of end
len_sovr  = sovr_ze-sovr_cle;                   % single overlap length
SOVFThick = len_sovr.*2./(len_thick-len_hbare); % thick filament overlap frac
SOVFThin  = len_sovr./len_thin;                 % thin filament overlap frac

% Compute combined Ca binding to high- (w./XB) and low- (no XB) sites
Tropreg = (1-SOVFThin).*TRPNCaL + SOVFThin.*TRPNCaH;
permtot = sqrt(1./(1+(perm50./Tropreg).^nperm));
inprmt  = min(1./permtot, 100);

% Adjustments for Ca activation, temperature, SL, stress and strain
konT    = kon.*Qkon.^((Temp-310)./10);
hfmd    = exp(-sign(xXBprer).*hfmdc.*((xXBprer./x_0).^2));
hbmd    = exp(sign((xXBpostr-x_0)).*hbmdc.*(((xXBpostr-x_0)./x_0).^2));

% Adjustments for Ca activation, temperature, SL, stress and strain
kn_pT   = kn_p.*permtot.*Qkn_p.^((Temp-310)./10);
kp_nT   = kp_n.*inprmt.*Qkp_n.^((Temp-310)./10);
fappT   = fapp.*xbmodsp.*Qfapp.^((Temp-310)./10);
gapslmd = 1 + (1-SOVFThick).*gslmod;
gappT   = gapp.*gapslmd.*xbmodsp.*Qgapp.^((Temp-310)./10);
hfT     = hfXB.*hfmd.*xbmodsp.*Qhf.^((Temp-310)./10);
hbT     = hb.*hbmd.*xbmodsp.*Qhb.^((Temp-310)./10);
koffLT  = koffL.*Qkoff.^((Temp-310)./10).*koffmod;
koffHT  = koffH.*Qkoff.^((Temp-310)./10).*koffmod;

% steady-state fractions in XBprer and XBpostr using King-Altman rule
SSXBprer = (hb.*fapp+gxb.*fapp)./...
  (gxb.*hfXB+fapp.*hfXB+gxb.*gapp+hb.*fapp+hb.*gapp+gxb.*fapp);
SSXBpostr = fapp.*hfXB./(gxb.*hfXB+fapp.*hfXB+gxb.*gapp+hb.*fapp+hb.*gapp+gxb.*fapp);

% normalization for scaling active and passive force (maximal force)
Fnordv = kxb.*x_0.*SSXBpostr;

% Calculate Forces (active, passive, preload, afterload)
force = kxb.*SOVFThick.*(xXBpostr.*XBpostr+xXBprer.*XBprer);
active = force./Fnordv;
ppforce_t = sign(SL-SLrest).*PCon_t.*(exp(PExp_t.*abs(SL-SLrest))-1);
if (SL-SL_c) < 0 
        ppforce_c = 0.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
elseif (SL-SL_c) == 0 
    ppforce_c = 1./2.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
else
    ppforce_c = 1.*PCon_c.*(exp(PExp_c.*abs(SL-SL_c))-1);
end
ppforce = ppforce_t + ppforce_c;
preload = sign(SLset-SLrest).*PCon_t.*(exp(PExp_t.*abs(SLset-SLrest))-1);
afterload = zeros(size(SL));%KSE./100.*(SLset-SL);

%% change in SL and forces
lmbda = SL./SLset;

dintf = (-ppforce+preload-active+afterload);

dSL = 0;


sigma = 6;
if t<=640
    Gauss = 640;
    GaussCurve = -1./(sqrt(8.9.*pi)) .* (-1/2.*(2*(t-(Gauss))./sigma)*1./sigma) .* exp(-1/2.*((t-(Gauss))./sigma).^2);
elseif t>640 && t<=840
    GaussCurve = 0;
else
    Gauss = 840;
    GaussCurve = -1./(sqrt(8.9.*pi)) .* (-1/2.*(2*(t-(Gauss))./sigma)*1./sigma) .* exp(-1/2.*((t-(Gauss))./sigma).^2);
end

dSL = 0;%dSL + GaussCurve;

%% change to Rice model
sigman = sigman.*exp(konFac2*(lmbda-1));
sigmap = sigmap.*exp(koffFac2*(lmbda-1));
gxbmd   = heaviside(x_0-xXBpostr).*exp(sigmap.*((x_0-xXBpostr)./x_0).^2)+...
  (1-heaviside(x_0-xXBpostr)).*exp(sigman.*(((xXBpostr-x_0)./x_0).^2));

gxbT    = gxb.*gxbmd.*xbmodsp.*Qgxb.^((Temp-310)./10);

%%
% Regulation and corssbridge cycling state derivatives
dTRPNCaL  = konT.*(exp(k_on*(lmbda-1))).*CaiMech.*(1-TRPNCaL) - koffLT.*(exp(k_off*(lmbda-1))^(-1)).*TRPNCaL;
dTRPNCaH  = konT.*(exp(k_on*(lmbda-1))).*CaiMech.*(1-TRPNCaH) - koffHT.*(exp(k_off*(lmbda-1))^(-1)).*TRPNCaH;
%%

dN_NoXB   = -kn_pT.*N_NoXB+ kp_nT.*P_NoXB;
dP_NoXB   = -kp_nT.*P_NoXB + kn_pT.*N_NoXB;

dN        = -kn_pT.*N+ kp_nT.*P;
% dP      = -kp_nT.*P + kn_pT.*N - fappT.*P + gappT.*XBprer + gxbT.*XBpostr;
dXBprer   = fappT.*P - gappT.*XBprer - hfT.*XBprer + hbT.*XBpostr;
dXBpostr  = hfT.*XBprer - hbT.*XBpostr - gxbT.*XBpostr;

dP        = -(dN+dXBprer+dXBpostr);

% Mean strain of strongly-bound states due to SL motion and XB cycling
dutyprer  = (hbT.*fappT+gxbT.*fappT)./...    % duty fractions using the
  (fappT.*hfT+gxbT.*hfT+gxbT.*gappT+hbT.*fappT+hbT.*gappT+gxbT.*fappT);
dutypostr = fappT.*hfT./...                 % King-Alman Rule    
  (fappT.*hfT+gxbT.*hfT+gxbT.*gappT+hbT.*fappT+hbT.*gappT+gxbT.*fappT);
dxXBprer = 0.5.*dSL+xPsi./dutyprer.*(-xXBprer.*fappT+(xXBpostr-x_0-xXBprer).*hbT);
dxXBpostr = 0.5.*dSL+ xPsi./dutypostr.*(x_0+xXBprer-xXBpostr).*hfT;

% Ca buffering by low-affinity troponin C (LTRPNCa)
FrSBXB    = (XBpostr+XBprer)./(SSXBpostr + SSXBprer);
dFrSBXB   = (dXBpostr+dXBprer)./(SSXBpostr + SSXBprer);

if (len_thick-SL) < 0 
    dsovr_ze  = -dSL./2.*0;
elseif (len_thick-SL) == 0 
    dsovr_ze  = -dSL./2.*1./2;
else
    dsovr_ze  = -dSL./2.*1;
end

if ((2.*len_thin-SL)-len_hbare) < 0
    dsovr_cle = -dSL./2.*0;
elseif ((2.*len_thin-SL)-len_hbare) == 0
    dsovr_cle = -dSL./2.*1./2;
else
    dsovr_cle = -dSL./2.*1;
end

dlen_sovr = dsovr_ze-dsovr_cle;
dSOVFThin = dlen_sovr./len_thin;
dSOVFThick= 2.*dlen_sovr./(len_thick-len_hbare);

TropTot = Trop_conc.*((1-SOVFThin).*TRPNCaL + ...
  SOVFThin.*(FrSBXB.*TRPNCaH+(1-FrSBXB).*TRPNCaL));
dTropTot= (Trop_conc.*(-dSOVFThin.*TRPNCaL+(1-SOVFThin).*dTRPNCaL + ...
  dSOVFThin.*(FrSBXB.*TRPNCaH+(1-FrSBXB).*TRPNCaL) + ...
  SOVFThin.*(dFrSBXB.*TRPNCaH+FrSBXB.*dTRPNCaH-dFrSBXB.*TRPNCaL+...
  (1-FrSBXB).*dTRPNCaL)));


dforce = kxb.*dSOVFThick.*(xXBpostr.*XBpostr+xXBprer.*XBprer) + ...
  kxb.*SOVFThick.*(dxXBpostr.*XBpostr+xXBpostr.*dXBpostr + ...
  dxXBprer.*XBprer+xXBprer.*dXBprer);

dactive = 0.5.*dforce./Fnordv;
dppforce_t = sign(SL-SLrest).*PCon_t.*PExp_t.*dSL.*exp(PExp_t.*abs(SL-SLrest));

if (SL-SL_c) < 0
    dppforce_c = 0.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
elseif (SL-SL_c) == 0
    dppforce_c = 1./2.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
else
    dppforce_c = 1.*PCon_c.*PExp_c.*dSL.*exp(PExp_c.*abs(SL-SL_c));
end

dppforce = dppforce_t + dppforce_c;
dsfib = dppforce+dactive;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
epi=1;

% Constants
R = 8314;       % [J/kmol*K]  
Frdy = 96485;   % [C/mol]  
Temp = 310;     % [K]
FoRT = Frdy/R/Temp;
Cmem = 1.3810e-10;   % [F] membrane capacitance
Qpow = (Temp-310)/10;

% Cell geometry
cellLength = 100;     % cell length [um]
cellRadius = 10.25;   % cell radius [um]
junctionLength = 160e-3;  % junc length [um]
junctionRadius = 15e-3;   % junc radius [um]
distSLcyto = 0.45;    % dist. SL to cytosol [um]
distJuncSL = 0.5;  % dist. junc to SL [um]
DcaJuncSL = 1.64e-6;  % Dca junc to SL [cm^2/sec]
DcaSLcyto = 1.22e-6; % Dca SL to cyto [cm^2/sec]
DnaJuncSL = 1.09e-5;  % Dna junc to SL [cm^2/sec]
DnaSLcyto = 1.79e-5;  % Dna SL to cyto [cm^2/sec] 
Vcell = pi*cellRadius^2*cellLength*1e-15;    % [L]
Vmyo = 0.65*Vcell; Vsr = 0.035*Vcell; Vsl = 0.02*Vcell; Vjunc = 0.0539*.01*Vcell; 
SAjunc = 20150*pi*2*junctionLength*junctionRadius;  % [um^2]
SAsl = pi*2*cellRadius*cellLength;          % [um^2]
J_ca_juncsl =1/1.2134e12; % [L/msec] = 8.2413e-13
J_ca_slmyo = 1/2.68510e11; % [L/msec] = 3.2743e-12
J_na_juncsl = 1/(1.6382e12/3*100); % [L/msec] = 6.1043e-13
J_na_slmyo = 1/(1.8308e10/3*100);  % [L/msec] = 5.4621e-11

% Fractional currents in compartments
Fjunc = 0.11;   Fsl = 1-Fjunc;
Fjunc_CaL = 0.9; Fsl_CaL = 1-Fjunc_CaL;

% Fixed ion concentrations     
Cli = 15;   % Intracellular Cl  [mM]
Clo = 150;  % Extracellular Cl  [mM]
Ko = 5.4;   % Extracellular K   [mM]
Nao = 140;  % Extracellular Na  [mM]
Cao = 1.8;  % Extracellular Ca  [mM]1.8
Mgi = 1;    % Intracellular Mg  [mM]

% Nernst Potentials
ena_junc = (1/FoRT)*log(Nao/y(32));     % [mV]
ena_sl = (1/FoRT)*log(Nao/y(33));       % [mV]
% ena_junc = (1/FoRT)*log(Nao/7.65);     % [mV]
% ena_sl = (1/FoRT)*log(Nao/7.65);       % [mV]
ek = (1/FoRT)*log(Ko/y(35));	        % [mV]
eca_junc = (1/FoRT/2)*log(Cao/y(36));   % [mV]
eca_sl = (1/FoRT/2)*log(Cao/y(37));     % [mV]
ecl = (1/FoRT)*log(Cli/Clo);            % [mV]

% Na transport parameters
%GNa =23;        % [mS/uF]
%%
GNa=23;
GNaB = 0.597e-3;    % [mS/uF] 0.897e-3
IbarNaK = 1.0*1.8;%1.90719;     % [uA/uF]
KmNaip = 11;         % [mM]11
KmKo =1.5;         % [mM]1.5
Q10NaK = 1.63;  
Q10KmNai = 1.39;

%% K current parameters
pNaK = 0.01833;      
gkp = 2*0.001;

% Cl current parameters
GClCa =0.5* 0.109625;   % [mS/uF]
GClB = 1*9e-3;        % [mS/uF]
KdClCa = 100e-3;    % [mM]

% I_Ca parameters
pNa = 0.50*1.5e-8;       % [cm/sec]
pCa = 0.50*5.4e-4;       % [cm/sec]
pK = 0.50*2.7e-7;        % [cm/sec]
Q10CaL = 1.8;       

%% Ca transport parameters
IbarNCX = 1.0*4.5;      % [uA/uF]5.5 before - 9 in rabbit
KmCai = 3.59e-3;    % [mM]
KmCao = 1.3;        % [mM]
KmNai = 12.29;      % [mM]
KmNao = 87.5;       % [mM]
ksat = 0.32;        % [none]  
nu = 0.27;          % [none]
Kdact = 0.150e-3;   % [mM] 
Q10NCX = 1.57;      % [none]
IbarSLCaP = 0.0673; % IbarSLCaP FEI changed [uA/uF](2.2 umol/L cytosol/sec) jeff 0.093 [uA/uF]
KmPCa = 0.5e-3;     % [mM] 
GCaB = 5.513e-4;    % [uA/uF] 3
Q10SLCaP = 2.35;    % [none]

% SR flux parameters
Q10SRCaP = 2.6;          % [none]
Vmax_SRCaP = 1.0*5.3114e-3;  % [mM/msec] (286 umol/L cytosol/sec)
Kmf = 0.246e-3;          % [mM] default
%Kmf = 0.175e-3;          % [mM]
Kmr = 1.7;               % [mM]L cytosol
hillSRCaP = 1.787;       % [mM]
ks = 25;                 % [1/ms]      
koCa = 10;               % [mM^-2 1/ms]   %default 10   modified 20
kom = 0.06;              % [1/ms]     
kiCa = 0.5;              % [1/mM/ms]
kim = 0.005;             % [1/ms]
ec50SR = 0.45;           % [mM]

% Buffering parameters
% koff: [1/s] = 1e-3*[1/ms];  kon: [1/uM/s] = [1/mM/ms]
Bmax_Naj = 7.561;       % [mM] % Bmax_Naj = 3.7; (c-code difference?)  % Na buffering
Bmax_Nasl = 1.65;       % [mM]
koff_na = 1e-3;         % [1/ms]
kon_na = 0.1e-3;        % [1/mM/ms]
Bmax_TnClow = 70e-3;    % [mM]                      % TnC low affinity
koff_tncl = 19.6e-3;    % [1/ms] 
kon_tncl = 32.7;        % [1/mM/ms]
Bmax_TnChigh = 140e-3;  % [mM]                      % TnC high affinity 
koff_tnchca = 0.032e-3; % [1/ms] 
kon_tnchca = 2.37;      % [1/mM/ms]
koff_tnchmg = 3.33e-3;  % [1/ms] 
kon_tnchmg = 3e-3;      % [1/mM/ms]
Bmax_CaM = 24e-3;       % [mM] **? about setting to 0 in c-code**   % CaM buffering
koff_cam = 238e-3;      % [1/ms] 
kon_cam = 34;           % [1/mM/ms]
Bmax_myosin = 140e-3;   % [mM]                      % Myosin buffering
koff_myoca = 0.46e-3;   % [1/ms]
kon_myoca = 13.8;       % [1/mM/ms]
koff_myomg = 0.057e-3;  % [1/ms]
kon_myomg = 0.0157;     % [1/mM/ms]
Bmax_SR = 19*.9e-3;     % [mM] (Bers text says 47e-3) 19e-3
koff_sr = 60e-3;        % [1/ms]
kon_sr = 100;           % [1/mM/ms]
Bmax_SLlowsl = 37.4e-3*Vmyo/Vsl;        % [mM]    % SL buffering
Bmax_SLlowj = 4.6e-3*Vmyo/Vjunc*0.1;    % [mM]    %Fei *0.1!!! junction reduction factor
koff_sll = 1300e-3;     % [1/ms]
kon_sll = 100;          % [1/mM/ms]
Bmax_SLhighsl = 13.4e-3*Vmyo/Vsl;       % [mM] 
Bmax_SLhighj = 1.65e-3*Vmyo/Vjunc*0.1;  % [mM] %Fei *0.1!!! junction reduction factor
koff_slh = 30e-3;       % [1/ms]
kon_slh = 100;          % [1/mM/ms]
Bmax_Csqn = 140e-3*Vmyo/Vsr;            % [mM] % Bmax_Csqn = 2.6;      % Csqn buffering
koff_csqn = 65;         % [1/ms] 
kon_csqn = 100;         % [1/mM/ms] 


%% Membrane Currents
% I_Na: Fast Na Current
mss = 1 / ((1 + exp( -(56.86 + y(39)) / 9.03 ))^2);
taum = 0.1292 * exp(-((y(39)+45.79)/15.54)^2) + 0.06487 * exp(-((y(39)-4.823)/51.12)^2);                 
 
ah = (y(39) >= -40) * (0)... 
   + (y(39) < -40) * (0.057 * exp( -(y(39) + 80) / 6.8 )); 
bh = (y(39) >= -40) * (0.77 / (0.13*(1 + exp( -(y(39) + 10.66) / 11.1 )))) ...
   + (y(39) < -40) * ((2.7 * exp( 0.079 * y(39)) + 3.1*10^5 * exp(0.3485 * y(39)))); 
tauh = 1 / (ah + bh); 
hss = 1 / ((1 + exp( (y(39) + 71.55)/7.43 ))^2);
 
aj = (y(39) >= -40) * (0) ...
    +(y(39) < -40) * (((-2.5428 * 10^4*exp(0.2444*y(39)) - 6.948*10^-6 * exp(-0.04391*y(39))) * (y(39) + 37.78)) / ...
                     (1 + exp( 0.311 * (y(39) + 79.23) )));
bj = (y(39) >= -40) * ((0.6 * exp( 0.057 * y(39))) / (1 + exp( -0.1 * (y(39) + 32) ))) ...
   + (y(39) < -40) * ((0.02424 * exp( -0.01052 * y(39) )) / (1 + exp( -0.1378 * (y(39) + 40.14) ))); 
tauj = 1 / (aj + bj);
jss = 1 / ((1 + exp( (y(39) + 71.55)/7.43 ))^2);         
 
ydot(1) = (mss - y(1)) / taum;
ydot(2) = (hss - y(2)) / tauh;
ydot(3) = (jss - y(3)) / tauj;
    
I_Na_junc = Fjunc*GNa*y(1)^3*y(2)*y(3)*(y(39)-ena_junc);
I_Na_sl = Fsl*GNa*y(1)^3*y(2)*y(3)*(y(39)-ena_sl);
I_Na = I_Na_junc+I_Na_sl;

% I_nabk: Na Background Current
I_nabk_junc = Fjunc*GNaB*(y(39)-ena_junc);
I_nabk_sl = Fsl*GNaB*(y(39)-ena_sl);
I_nabk = I_nabk_junc+I_nabk_sl;

% I_nak: Na/K Pump Current
sigma = (exp(Nao/67.3)-1)/7;
fnak = 1/(1+0.1245*exp(-0.1*y(39)*FoRT)+0.0365*sigma*exp(-y(39)*FoRT));
I_nak_junc = 1*Fjunc*IbarNaK*fnak*Ko /(1+(KmNaip/y(32))^4) /(Ko+KmKo);
I_nak_sl = 1*Fsl*IbarNaK*fnak*Ko /(1+(KmNaip/y(33))^4) /(Ko+KmKo);
I_nak = I_nak_junc+I_nak_sl;

%% I_kr: Rapidly Activating K Current
gkr =1.0*0.035*sqrt(Ko/5.4);
xrss = 1/(1+exp(-(y(39)+10)/5));
tauxr = 550/(1+exp((-22-y(39))/9))*6/(1+exp((y(39)-(-11))/9))+230/(1+exp((y(39)-(-40))/20));
ydot(12) = (xrss-y(12))/tauxr;
rkr = 1/(1+exp((y(39)+74)/24));

I_kr = gkr*y(12)*rkr*(y(39)-ek);

%% I_ks: Slowly Activating K Current
markov_iks=0;    

eks = (1/FoRT)*log((Ko+pNaK*Nao)/(y(35)+pNaK*y(34)));

if markov_iks==0;
gks_junc=1*0.0035;
gks_sl=1*0.0035; %FRA
xsss = 1 / (1+exp(-(y(39) + 3.8)/14.25)); % fitting Fra
tauxs=990.1/(1+exp(-(y(39)+2.436)/14.12));
ydot(13) = (xsss-y(13))/tauxs;
I_ks_junc = Fjunc*gks_junc*y(13)^2*(y(39)-eks);
I_ks_sl = Fsl*gks_sl*y(13)^2*(y(39)-eks);                                                                                                                                   
I_ks = I_ks_junc+I_ks_sl;
else
    gks_junc=0.0065;
    gks_sl=0.0065; %FRA
    alpha=3.98e-4*exp(3.61e-1*y(39)*FoRT);
    beta=5.74e-5*exp(-9.23e-2*y(39)*FoRT);
    gamma=3.41e-3*exp(8.68e-1*y(39)*FoRT);
    delta=1.2e-3*exp(-3.3e-1*y(39)*FoRT);
    teta=6.47e-3;
    eta=1.25e-2*exp(-4.81e-1*y(39)*FoRT);
    psi=6.33e-3*exp(1.27*y(39)*FoRT);
    omega=4.91e-3*exp(-6.79e-1*y(39)*FoRT);
    
ydot(42)=-4*alpha*y(42)+beta*y(43);
ydot(43)=4*alpha*y(42)-(beta+gamma+3*alpha)*y(43)+2*beta*y(44);
ydot(44)=3*alpha*y(43)-(2*beta+2*gamma+2*alpha)*y(44)+3*beta*y(45);
ydot(45)=2*alpha*y(44)-(3*beta+3*gamma+alpha)*y(45)+4*beta*y(46);
ydot(46)=1*alpha*y(44)-(4*beta+4*gamma)*y(46)+delta*y(50);    
ydot(47)=gamma*y(43)-(delta+3*alpha)*y(47)+beta*y(48);   
ydot(48)=2*gamma*y(44)+3*alpha*y(47)-(delta+beta+2*alpha+gamma)*y(48)+2*beta*y(49)+2*delta*y(51);
ydot(49)=3*gamma*y(45)+2*alpha*y(48)-(delta+2*beta+1*alpha+2*gamma)*y(49)+3*beta*y(50)+2*delta*y(52);
ydot(50)=4*gamma*y(46)+1*alpha*y(49)-(delta+3*beta+0*alpha+3*gamma)*y(50)+2*delta*y(53);
ydot(51)=1*gamma*y(48)-(2*delta+2*alpha)*y(51)+beta*y(52);  
ydot(52)=2*gamma*y(49)+2*alpha*y(51)-(2*delta+beta+1*alpha+gamma)*y(52)+2*beta*y(53)+3*delta*y(54);
ydot(53)=3*gamma*y(50)+1*alpha*y(52)-(2*delta+2*beta+2*gamma)*y(53)+3*delta*y(55);
ydot(54)=1*gamma*y(52)-(3*delta+1*alpha)*y(54)+beta*y(55);  
ydot(55)=2*gamma*y(53)+1*alpha*y(54)-(3*delta+1*beta+1*gamma)*y(55)+4*delta*y(56);
ydot(56)=1*gamma*y(55)-(4*delta+teta)*y(56)+eta*y(57);
O2=1-(y(42)+y(43)+y(44)+y(45)+y(46)+y(47)+y(49)+y(48)+y(50)+y(51)+y(52)+y(53)+y(54)+y(55)+y(56)+y(57));
ydot(57)=1*teta*y(56)-(eta+psi)*y(57)+omega*O2;
I_ks_junc = Fjunc*gks_junc*(y(57)+O2)*(y(39)-eks);
I_ks_sl = Fsl*gks_sl*(y(57)+O2)*(y(39)-eks);                                                                                                                                   
I_ks = I_ks_junc+I_ks_sl;
end
%I_kp: Plateau K current
kp_kp = 1/(1+exp(7.488-y(39)/5.98));
I_kp_junc = Fjunc*gkp*kp_kp*(y(39)-ek);
I_kp_sl = Fsl*gkp*kp_kp*(y(39)-ek);
I_kp = I_kp_junc+I_kp_sl;

%% I_to: Transient Outward K Current (slow and fast components)
% modified for human myocytes
if epi==1;
GtoSlow=1.0*0.13*0.12; %epi
GtoFast=1.0*0.13*0.88; %epi0.88
else
GtoSlow=0.13*0.3*0.964; %endo
GtoFast=0.13*0.3*0.036; %endo
end

xtoss = 1/(1+exp(-(y(39)-19.0)/13));
ytoss = 1/(1+exp((y(39)+19.5)/5));
% rtoss = 1/(1+exp((y(39)+33.5)/10));
tauxtos = 9/(1+exp((y(39)+3.0)/15))+0.5;
tauytos = 800/(1+exp((y(39)+60.0)/10))+30;
% taurtos = 2.8e3/(1+exp((y(39)+60.0)/10))+220; %Fei changed here!! time-dependent gating variable
ydot(8) = (xtoss-y(8))/tauxtos;
ydot(9) = (ytoss-y(9))/tauytos;
% ydot(40)=0;
I_tos = GtoSlow*y(8)*y(9)*(y(39)-ek);    % [uA/uF]

tauxtof = 8.5*exp(-((y(39)+45)/50)^2)+0.5;
%tauxtof = 3.5*exp(-((y(39)+3)/30)^2)+1.5;
tauytof = 85*exp((-(y(39)+40)^2/220))+7;
%tauytof = 20.0/(1+exp((y(39)+33.5)/10))+20.0;
ydot(10) = (xtoss-y(10))/tauxtof;
ydot(11) = (ytoss-y(11))/tauytof;
I_tof = GtoFast*y(10)*y(11)*(y(39)-ek);
I_to = I_tos + I_tof;

%% I_ki: Time-Independent K Current
aki = 1.02/(1+exp(0.2385*(y(39)-ek-59.215)));
bki =(0.49124*exp(0.08032*(y(39)+5.476-ek)) + exp(0.06175*(y(39)-ek-594.31))) /(1 + exp(-0.5143*(y(39)-ek+4.753)));
% Ak1=0.1/(1+exp(0.06*(y(39)-ek-200)));
% Bk1=(3*exp(0.0002*(y(39)-ek+100))+ exp(0.1*(y(39)-ek-10)))/(1+exp(-0.5*(y(39)-ek)));
% kiss=Ak1/(Ak1+Bk1);
% I_ki = GK1*sqrt(Ko/5.4)*kiss*(y(39)-ek);
kiss = aki/(aki+bki);
I_ki =1* 0.35*sqrt(Ko/5.4)*kiss*(y(39)-ek);

% I_ClCa: Ca-activated Cl Current, I_Clbk: background Cl Current
I_ClCa_junc = Fjunc*GClCa/(1+KdClCa/y(36))*(y(39)-ecl);
I_ClCa_sl = Fsl*GClCa/(1+KdClCa/y(37))*(y(39)-ecl);
I_ClCa = I_ClCa_junc+I_ClCa_sl;
I_Clbk = GClB*(y(39)-ecl);

% %% I_Ca: L-type Calcium Current
fss = 1/(1+exp((y(39)+35)/9))+0.6/(1+exp((50-y(39))/20));

%% I_Ca: L-type Calcium Current
dss = 1/(1+exp(-(y(39)+5)/6.0));
taud = dss*(1-exp(-(y(39)+5)/6.0))/(0.035*(y(39)+5));
% fss = 1/(1+exp((y(39)+35.06)/3.6))+0.6/(1+exp((50-y(39))/20));
tauf = 1/(0.0197*exp( -(0.0337*(y(39)+14.5))^2 )+0.02);
ydot(4) = (dss-y(4))/taud;
ydot(5) = (fss-y(5))/tauf;
ydot(6) = 1.7*y(36)*(1-y(6))-11.9e-3*y(6); % fCa_junc
ydot(7) = 1.7*y(37)*(1-y(7))-11.9e-3*y(7); % fCa_sl
fcaCaMSL= 0.1/(1+(0.01/y(37)));
fcaCaj= 0.1/(1+(0.01/y(36)));
fcaCaMSL=0;
fcaCaj= 0;
ibarca_j = pCa*4*(y(39)*Frdy*FoRT) * (0.341*y(36)*exp(2*y(39)*FoRT)-0.341*Cao) /(exp(2*y(39)*FoRT)-1);
ibarca_sl = pCa*4*(y(39)*Frdy*FoRT) * (0.341*y(37)*exp(2*y(39)*FoRT)-0.341*Cao) /(exp(2*y(39)*FoRT)-1);
ibark = pK*(y(39)*Frdy*FoRT)*(0.75*y(35)*exp(y(39)*FoRT)-0.75*Ko) /(exp(y(39)*FoRT)-1);
ibarna_j = pNa*(y(39)*Frdy*FoRT) *(0.75*y(32)*exp(y(39)*FoRT)-0.75*Nao)  /(exp(y(39)*FoRT)-1);
ibarna_sl = pNa*(y(39)*Frdy*FoRT) *(0.75*y(33)*exp(y(39)*FoRT)-0.75*Nao)  /(exp(y(39)*FoRT)-1);
I_Ca_junc = (Fjunc_CaL*ibarca_j*y(4)*y(5)*((1-y(6))+fcaCaj)*Q10CaL^Qpow)*0.45*1;
I_Ca_sl = (Fsl_CaL*ibarca_sl*y(4)*y(5)*((1-y(7))+fcaCaMSL)*Q10CaL^Qpow)*0.45*1;
I_Ca = I_Ca_junc+I_Ca_sl;
I_CaK = (ibark*y(4)*y(5)*(Fjunc_CaL*(fcaCaj+(1-y(6)))+Fsl_CaL*(fcaCaMSL+(1-y(7))))*Q10CaL^Qpow)*0.45*1;
I_CaNa_junc = (Fjunc_CaL*ibarna_j*y(4)*y(5)*((1-y(6))+fcaCaj)*Q10CaL^Qpow)*0.45*1;
I_CaNa_sl = (Fsl_CaL*ibarna_sl*y(4)*y(5)*((1-y(7))+fcaCaMSL)*Q10CaL^Qpow)*.45*1;
I_CaNa = I_CaNa_junc+I_CaNa_sl;
I_Catot = I_Ca+I_CaK+I_CaNa;

% I_ncx: Na/Ca Exchanger flux
Ka_junc = 1/(1+(Kdact/y(36))^2);
Ka_sl = 1/(1+(Kdact/y(37))^2);
s1_junc = exp(nu*y(39)*FoRT)*y(32)^3*Cao;
s1_sl = exp(nu*y(39)*FoRT)*y(33)^3*Cao;
s2_junc = exp((nu-1)*y(39)*FoRT)*Nao^3*y(36);
s3_junc = KmCai*Nao^3*(1+(y(32)/KmNai)^3) + KmNao^3*y(36)*(1+y(36)/KmCai)+KmCao*y(32)^3+y(32)^3*Cao+Nao^3*y(36);
s2_sl = exp((nu-1)*y(39)*FoRT)*Nao^3*y(37);
s3_sl = KmCai*Nao^3*(1+(y(33)/KmNai)^3) + KmNao^3*y(37)*(1+y(37)/KmCai)+KmCao*y(33)^3+y(33)^3*Cao+Nao^3*y(37);

I_ncx_junc = Fjunc*IbarNCX*Q10NCX^Qpow*Ka_junc*(s1_junc-s2_junc)/s3_junc/(1+ksat*exp((nu-1)*y(39)*FoRT));
I_ncx_sl = Fsl*IbarNCX*Q10NCX^Qpow*Ka_sl*(s1_sl-s2_sl)/s3_sl/(1+ksat*exp((nu-1)*y(39)*FoRT));
I_ncx = I_ncx_junc+I_ncx_sl;

% I_pca: Sarcolemmal Ca Pump Current
I_pca_junc = Fjunc*Q10SLCaP^Qpow*IbarSLCaP*y(36)^1.6/(KmPCa^1.6+y(36)^1.6);
I_pca_sl = Fsl*Q10SLCaP^Qpow*IbarSLCaP*y(37)^1.6/(KmPCa^1.6+y(37)^1.6);
I_pca = I_pca_junc+I_pca_sl;

% I_cabk: Ca Background Current
I_cabk_junc = Fjunc*GCaB*(y(39)-eca_junc);
I_cabk_sl = Fsl*GCaB*(y(39)-eca_sl);
I_cabk = I_cabk_junc+I_cabk_sl;

%% SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
MaxSR = 15; MinSR = 1;
kCaSR = MaxSR - (MaxSR-MinSR)/(1+(ec50SR/y(31))^2.5);
koSRCa = koCa/kCaSR;
kiSRCa = kiCa*kCaSR;
RI = 1-y(14)-y(15)-y(16);
ydot(14) = (kim*RI-kiSRCa*y(36)*y(14))-(koSRCa*y(36)^2*y(14)-kom*y(15));   % R
ydot(15) = (koSRCa*y(36)^2*y(14)-kom*y(15))-(kiSRCa*y(36)*y(15)-kim*y(16));% O
ydot(16) = (kiSRCa*y(36)*y(15)-kim*y(16))-(kom*y(16)-koSRCa*y(36)^2*RI);   % I
J_SRCarel = ks*y(15)*(y(31)-y(36));          % [mM/ms]
J_serca = 1*Q10SRCaP^Qpow*Vmax_SRCaP*((y(38)/Kmf)^hillSRCaP-(y(31)/Kmr)^hillSRCaP)...
    /(1+(y(38)/Kmf)^hillSRCaP+(y(31)/Kmr)^hillSRCaP);
J_SRleak = 5.348e-6*(y(31)-y(36));           %   [mM/ms]


%% Sodium and Calcium Buffering
ydot(17) = kon_na*y(32)*(Bmax_Naj-y(17))-koff_na*y(17);        % NaBj      [mM/ms]
ydot(18) = kon_na*y(33)*(Bmax_Nasl-y(18))-koff_na*y(18);       % NaBsl     [mM/ms]

% Cytosolic Ca Buffers
% ydot(19) = kon_tncl*y(38)*(Bmax_TnClow-y(19))-koff_tncl*y(19);            % TnCL      [mM/ms]
ydot(19) = Bmax_TnClow*dTropTot/Trop_conc;            % TnCL      [mM/ms]
ydot(20) = kon_tnchca*y(38)*(Bmax_TnChigh-y(20)-y(21))-koff_tnchca*y(20); % TnCHc     [mM/ms]
ydot(21) = kon_tnchmg*Mgi*(Bmax_TnChigh-y(20)-y(21))-koff_tnchmg*y(21);   % TnCHm     [mM/ms]
ydot(22) = kon_cam*y(38)*(Bmax_CaM-y(22))-koff_cam*y(22);                 % CaM       [mM/ms]
ydot(23) = kon_myoca*y(38)*(Bmax_myosin-y(23)-y(24))-koff_myoca*y(23);    % Myosin_ca [mM/ms]
ydot(24) = kon_myomg*Mgi*(Bmax_myosin-y(23)-y(24))-koff_myomg*y(24);      % Myosin_mg [mM/ms]
ydot(25) = kon_sr*y(38)*(Bmax_SR-y(25))-koff_sr*y(25);                    % SRB       [mM/ms]
J_CaB_cytosol = sum(ydot(19:25));

% Junctional and SL Ca Buffers
ydot(26) = kon_sll*y(36)*(Bmax_SLlowj-y(26))-koff_sll*y(26);       % SLLj      [mM/ms]
ydot(27) = kon_sll*y(37)*(Bmax_SLlowsl-y(27))-koff_sll*y(27);      % SLLsl     [mM/ms]
ydot(28) = kon_slh*y(36)*(Bmax_SLhighj-y(28))-koff_slh*y(28);      % SLHj      [mM/ms]
ydot(29) = kon_slh*y(37)*(Bmax_SLhighsl-y(29))-koff_slh*y(29);     % SLHsl     [mM/ms]
J_CaB_junction = ydot(26)+ydot(28);
J_CaB_sl = ydot(27)+ydot(29);

%% Ion concentrations
% SR Ca Concentrations
ydot(30) = kon_csqn*y(31)*(Bmax_Csqn-y(30))-koff_csqn*y(30);       % Csqn      [mM/ms]
ydot(31) = J_serca-(J_SRleak*Vmyo/Vsr+J_SRCarel)-ydot(30);         % Ca_sr     [mM/ms] %Ratio 3 leak current

% Sodium Concentrations
I_Na_tot_junc = I_Na_junc+I_nabk_junc+3*I_ncx_junc+3*I_nak_junc+I_CaNa_junc;   % [uA/uF]
I_Na_tot_sl = I_Na_sl+I_nabk_sl+3*I_ncx_sl+3*I_nak_sl+I_CaNa_sl;   % [uA/uF]
I_Na_tot_sl2 = 3*I_ncx_sl+3*I_nak_sl+I_CaNa_sl;   % [uA/uF]
I_Na_tot_junc2 = 3*I_ncx_junc+3*I_nak_junc+I_CaNa_junc;   % [uA/uF]

ydot(32) = -I_Na_tot_junc*Cmem/(Vjunc*Frdy)+J_na_juncsl/Vjunc*(y(33)-y(32))-ydot(17);
ydot(33) = -I_Na_tot_sl*Cmem/(Vsl*Frdy)+J_na_juncsl/Vsl*(y(32)-y(33))...
   +J_na_slmyo/Vsl*(y(34)-y(33))-ydot(18);
ydot(34) = J_na_slmyo/Vmyo*(y(33)-y(34));             % [mM/msec] 

% Potassium Concentration
I_K_tot = I_to+I_kr+I_ks+I_ki-2*I_nak+I_CaK+I_kp;     % [uA/uF]
ydot(35) =0; 

% Calcium Concentrations
I_Ca_tot_junc = I_Ca_junc+I_cabk_junc+I_pca_junc-2*I_ncx_junc;                   % [uA/uF]
I_Ca_tot_sl = I_Ca_sl+I_cabk_sl+I_pca_sl-2*I_ncx_sl;            % [uA/uF]
ydot(36) = -I_Ca_tot_junc*Cmem/(Vjunc*2*Frdy)+J_ca_juncsl/Vjunc*(y(37)-y(36))...
    -J_CaB_junction+(J_SRCarel)*Vsr/Vjunc+J_SRleak*Vmyo/Vjunc;  % Ca_j
ydot(37) = -I_Ca_tot_sl*Cmem/(Vsl*2*Frdy)+J_ca_juncsl/Vsl*(y(36)-y(37))...
    + J_ca_slmyo/Vsl*(y(38)-y(37))-J_CaB_sl;   % Ca_sl
ydot(38) = -J_serca*Vsr/Vmyo-J_CaB_cytosol +J_ca_slmyo/Vmyo*(y(37)-y(38));


%% Simulation type
if t>=10 && t<=13
    I_app = 9.5;
else
    I_app = 0.0;
end
 

%% Membrane Potential
%%
I_Na_tot = I_Na_tot_junc + I_Na_tot_sl;          % [uA/uF]
I_Cl_tot = I_ClCa+I_Clbk;                        % [uA/uF]
I_Ca_tot = I_Ca_tot_junc+I_Ca_tot_sl;
I_tot = I_Na_tot+I_Cl_tot+I_Ca_tot+I_K_tot;
%ydot(39) = -(I_Ca_tot+I_K_tot+I_Na_tot-I_app);
ydot(39) = -(I_tot-I_app);
vmax = ydot(39);

ydot(58)   = dN_NoXB;
ydot(59)     = dP_NoXB;
ydot(60)     = dN;
ydot(61)     = dP;
ydot(62)     = dXBprer;
ydot(63)     = dXBpostr;
ydot(64)     = dSL;
ydot(65)     = dxXBpostr;
ydot(66)     = dxXBprer;
ydot(67)     = dTRPNCaL;
ydot(68)     = dTRPNCaH;
ydot(69)     = dforce;
ydot(70)     = dintf;

% ----- END EC COUPLING MODEL ---------------
% adjust output depending on the function call
output = ydot;


%% Calculate timecourse for currents and other intermediates
function currents = calcCurrents(t,y,p)
% After running a simulation, feed the time vector and state variables into
% this function to compute ionic currents, etc.
% currents: [I_Na,I_Catot];
currents=[];
for i=1:size(t)
    if ceil(i/1000)==i/1000
        disp(['t = ',num2str(ceil(t(i)))]);
    end
    currents=[currents;f(t(i),y(i,:),p,'currents')];
end