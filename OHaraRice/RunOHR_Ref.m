function RunOHR_Ref

%initial conditions for state variables
v=-87;
nai=7;
nass=nai;
ki=145;
kss=ki;
cai=1.0e-4;
cass=cai;
cansr=1.2;
cajsr=cansr;
m=0;
hf=1;
hs=1;
j=1;
hsp=1;
jp=1;
mL=0;
hL=1;
hLp=1;
a=0;
iF=1;
iS=1;
ap=0;
iFp=1;
iSp=1;
d=0;
ff=1;
fs=1;
fcaf=1;
fcas=1;
jca=1;
nca=0;
ffp=1;
fcafp=1;
xrf=0;
xrs=0;
xs1=0;
xs2=0;
xk1=1;
Jrelnp=0;
Jrelp=0;
CaMKt=0;
%X0 is the vector for initial sconditions for state variables
X0=[v nai nass ki kss cai cass cansr cajsr m hf hs j hsp jp mL hL hLp a iF iS ap iFp iSp d ff fs fcaf fcas jca nca ffp fcafp xrf xrs xs1 xs2 xk1 Jrelnp Jrelp CaMKt]';


N_NoXB = 1.0;
P_NoXB = 0.0;
N = 0.97;

XBprer = 3.0494964880038e-7;
XBpostr = 1.81017564383744e-6;
SL = 1.85;
xXBpostr = 0.00700005394873882;
xXBprer = 3.41212828972468e-8; 
TRPNCaL = 0.0147730085063734;%0.017;%
TRPNCaH = 0.13066096561522;%0.14;%
force = -4.5113452510363e-6;
intf = 0;

P = 1-N-XBprer-XBpostr;


% y0 = [X0;N_NoXB;P_NoXB;N;P;XBprer;XBpostr;SL;xXBpostr;xXBprer;TRPNCaL;TRPNCaH;force;intf];
load OHR_Opti.mat
y0 = y(end,:);

load merged.mat


for beats = 1:200
    beats
    options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
    [t,y] = ode15s(@OHaraRice,[0:0.1:1000],y0,options,x(1),x(2),x(3),x(4),x(5),x(6),x(7),x(8),1);
    y0 = y(end,:);
end

save OHR_RefOpti